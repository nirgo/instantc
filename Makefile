# Makefile for InstantC
# Copyright (C) 2019 Joe Linhoff, see LICENSE file

.PHONY: all clean man install uninstall help tests

ifeq ($(OS), Windows_NT)
UNAME := Windows
else
UNAME := $(shell uname -s)
endif

ifneq ($(findstring Window,$(UNAME)),)
WINDOWS=1
SUDO=
INSTALLMANARGS:=-m 0644
else
SUDO=sudo
INSTALLMANARGS:=-g 0 -o 0 -m 0644
endif

APP=instantc
EC=@echo
LN=ln -sf
RM=rm -f
CP=cp

BASEDIR:=$(dir $(realpath $(firstword $(MAKEFILE_LIST))))
INSTALLBINDIR:=/usr/local/bin/
INSTALLDIR:=/usr/local/
MANDIR:=/usr/local/man/man1/
SCRIPTDIR:=scripts/
PRJDIR:=prj/
MANFILE:=$(APP).1
REBUILD:=+rebuild
CHMODX=chmod +x
RUN=instantc 

all clean:
	@make -C core -f make-local.mk $@
	@make -C prj/src -f make-local.mk $@
	@make -C scripts -f make-local.mk $@

man:
	$(EC) installing man page
	@$(SUDO) mkdir -p $(MANDIR)
	@$(SUDO) install $(INSTALLMANARGS) $(BASEDIR)prj/$(MANFILE) $(MANDIR)
	@$(SUDO) gzip -f $(MANDIR)$(MANFILE)

install: all man
	$(EC) installing $(APP)
	@$(CP) $(PRJDIR)dotinstantc $(HOME)/.instantc
	@$(CHMODX) $(SCRIPTDIR)*
	@if [ ! -f "$(INSTALLBINDIR)$(APP)" ]; then $(SUDO) $(LN) $(BASEDIR)bin/instantc $(INSTALLBINDIR)$(APP); fi
	@if [ ! -d "$(INSTALLDIR)$(APP)" ]; then $(SUDO) $(LN) $(BASEDIR) $(INSTALLDIR)$(APP); fi

uninstall:
	$(EC) uninstalling $(BASEDIR)
	@sudo $(RM) $(INSTALLBINDIR)$(APP)
	@sudo $(RM) $(INSTALLDIR)$(APP)
	@sudo $(RM) $(MANDIR)$(MANFILE)*
	@$(RM) $(HOME)/.instantc

help:
	$(EC) make all -- to rebuild all files 
	$(EC) make clean -- remove all files that were built
	$(EC) make install -- install into $(INSTALLBINDIR)
	$(EC) make uninstall -- install into $(INSTALLBINDIR)
	$(EC) make man -- install $(MANLOCAL) into $(MANDIR)$(MANFILE)
	$(EC) -- notes ----------
	$(EC) Install adds a sym link from $(INSTALLBINDIR)$(APP) to $(BASEDIR)

tests:
	$(EC) running tests
	@$(RUN)$(SCRIPTDIR)test_hello $(REBUILD)
	@$(RUN)$(SCRIPTDIR)test_sz $(REBUILD)
	@$(RUN)$(SCRIPTDIR)test_retcode $(REBUILD) 4 || [ $$? -eq 4 ]
	@$(RUN)$(SCRIPTDIR)test_corelib $(REBUILD)
	@$(RUN)$(SCRIPTDIR)test_corecpp $(REBUILD)
	@$(RUN)$(SCRIPTDIR)test_mathlib $(REBUILD)
	@$(RUN)$(SCRIPTDIR)test_shell $(REBUILD)
	@$(RUN)$(SCRIPTDIR)test_corecpp $(REBUILD)
	@$(RUN)$(SCRIPTDIR)test_beta1 $(REBUILD)
	@$(RUN)$(SCRIPTDIR)test_beta2 $(REBUILD) || [ $$? -eq 123 ]
	@$(RUN)$(SCRIPTDIR)test_beta3 $(REBUILD)
	@$(RUN)$(SCRIPTDIR)test_lib1 $(REBUILD)

# EOF
