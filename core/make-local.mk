# file: make-local.mk - Joe Linhoff
LIB=../lib/libcore.a

.PHONY: all clean help
NODEPS=clean help

ifeq ($(OS), Windows_NT)
UNAME:=Windows
else
UNAME:=$(shell uname -s)
endif

ifneq ($(findstring Window,$(UNAME)),)
WINDOWS=1
endif

all: $(LIB)

CC=gcc
AR=ar
LD=gcc
DEL=rm -f

CFLAGS=-I. -I.. -ggdb -g -DBUILD_PFM_LIB
ARFLAGS:=rc

SRC=$(wildcard *.c) # list of source files

SUFFIXES += .d
DEPFILES=$(patsubst %.c,%.d,$(SRC))

ifeq (1,$(WINDOWS))
CFLAGS+=-DBUILD_CYGWIN
else ifeq(Darwin,$(UNAME))
CFLAGS+=-DBUILD_DARWIN
else
CFLAGS+=-DBUILD_LINUX
endif

ifeq (0, $(words $(findstring $(MAKECMDGOALS), $(NODEPS))))
-include $(DEPFILES)
endif

$(LIB): $(SRC:%.c=%.o)
	@echo building $(LIB)
	$(AR) $(ARFLAGS) $@ $(SRC:%.c=%.o)

%.d: %.c
	$(CC) $(CFLAGS) -MM -MT '$(patsubst %.c,%.o,$<)' $< -MF $@

%.o: %.c %.d %.h
	$(CC) $(CFLAGS) -o $@ -c $<

clean:
	echo WINDOWS:$(WINDOWS)	
	$(DEL) $(LIB) *.o *.d

help:
	@echo make clean -- removes all built files
	@echo make -- build

