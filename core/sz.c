// sz.c
// Copyright (C) 2011-2018 JoeCo, All Rights Reserved.
#include <stdarg.h>
#include "lib/core.h"

// JFL 18 Mar 18
int szlen(cchr *s1,cchr *s1x)
{
  cchr *start;
  if(!s1) return 0;
  for(start=s1;(!s1x||(s1<s1x))&&*s1;s1++)
    /* do nothing */ ;
  return s1-start;
} // szlen()

// JFL 18 Mar 18
int szsize(cchr *s1,cchr *s1x)
{
  int r = szlen(s1,s1x);
  if(r) r++;
  return r;
} // szsize()

// JFL 17 Mar 18
cchr* szchr(cchr *s1,cchr *s1x,chr match)
{
  chr c;
  if(!s1) return 0;
  for(;(c=*s1) && (!s1x||(s1<s1x));s1++) {
    if(c == match)
      return s1;
  } // for
  return 0;
} // szchr()

// JFL 17 Sep 19
cchr* szrchr(cchr *s1,cchr *s1x,chr match)
{
  chr c;
  if(!s1) return 0;
  if(!s1x)
    s1x=s1+szlen(s1,0);

  for(s1x--;(s1<=s1x) && (c=*s1x);s1x--) {
    if(c == match)
      return s1x;
  } // for
  return 0;
} // szrchr()

// JFL 17 Mar 18
cchr* sztillwhite(cchr *s1,cchr *s1x)
{
  char c;
  for(;(!s1x||(s1<s1x))&&(c=*s1);s1++) {
    if(!C_IS_WHITE(c)) continue;
    break;
  } // for
  return s1; 
} // sztillwhite()

// JFL 17 Mar 18
cchr* szskipwhite(cchr *s1,cchr *s1x)
{
  char c;
  for(;(!s1x||(s1<s1x))&&(c=*s1);s1++) {
    if(C_IS_WHITE(c)) continue;
    break;
  } // for
  return s1; 
} // szskipwhite()

// JFL 23 Jun 19
cchr* sztrimwhite(cchr *s1,cchr *s1x)
{
  char c;
  if(!s1) return 0;
  if(!s1x) s1x=s1+szlen(s1,0);
  for(;s1<s1x;s1x--) {
    c=s1x[-1];
    if(C_IS_WHITE(c)) continue;
    break;
  } // for
  return s1x; 
} // sztrimwhite()

// JFL 09 Aug 19
cchr *szlastslash(cchr *s1,cchr *s1x)
{
  cchr *s2;
  if(!s1) return 0;
  while((s2=szchr(s1+1,s1x,'/'))||(s2=szchr(s1+1,s1x,'\\')))
    s1=s2;
  return ((*s1=='/') || (*s1=='\\')) ? s1 : 0;
} // szlastslash()

// JFL 17 Mar 18
cchr* szskipid(cchr *s1,cchr *s1x)
{
  char c;
  for(;(c=*s1) && (!s1x||(s1<s1x));s1++) {
    if(C_IS_ID(c)) continue;
    break;
  } // for
  return s1; 
} // szskipid()

// JFL 17 Mar 18
cchr* sztillid(cchr *s1,cchr *s1x)
{
  char c;
  for(;(!s1x||(s1<s1x))&&(c=*s1);s1++) {
    if(!C_IS_ID(c)) continue;
    break;
  } // for
  return s1; 
} // sztillid()

// JFL 18 Mar 18
cchr* szskipeol(cchr *s1,cchr *s1x)
{
  char c;
  for(;(!s1x||(s1<s1x))&&(c=*s1);s1++) {
    if(C_IS_EOL(c)) continue;
    break;
  } // for
  return s1; 
} // szskipeol()

// JFL 18 Mar 18
cchr* sztilleol(cchr *s1,cchr *s1x)
{
  char c;
  for(;(!s1x||(s1<s1x))&&(c=*s1);s1++) {
    if(!C_IS_EOL(c)) continue;
    break;
  } // for
  return s1; 
} // sztilleol()

// JFL 09 Nov 19
cchr* szskipchr(cchr *s1,cchr *s1x,cchr *skip,cchr *skipx)
{
  char c;
  cchr *match;
  if(!skipx) skipx=skip+szlen(skip,0);
  for(;(!s1x||(s1<s1x))&&(c=*s1);s1++) {
    for(match=skip;match<skipx;match++) {
      if(c == *match)
        goto match;
    } // for
    break; // not matched, done
    match:;
  } // for
  return s1; 
} // szskipchr()

// JFL 04 Jun 19
cchr* sztillchr(cchr *s1,cchr *s1x,cchr *till,cchr *tillx)
{
  char c;
  cchr *match;
  if(!tillx) tillx=till+szlen(till,0);
  for(;(!s1x||(s1<s1x))&&(c=*s1);s1++) {
    for(match=till;match<tillx;match++) {
      if(c == *match)
        goto done;
    } // for
  } // for
done:
  return s1; 
} // sztillchr()

// JFL 05 Nov 19
cchr* szskiparg(cchr *str,cchr*strx,cchr **s1p,cchr **s1xp)
{
  cchr *s1;
  if(((*str=='\"')||(*str=='\''))
    && (s1=szchr(str+1,strx,*str))) {
    if(s1p)
      *s1p=str+1;
    if(s1xp)
      *s1xp=s1;
    str=s1+1;
  } else {
    if(s1p)
      *s1p=str;
    str=sztillwhite(str,strx);
    if(s1xp)
      *s1xp=str;
  }
  return str;
} // szskiparg()

// JFL 28 May 19
int szcountlines(cchr *s1,cchr *end)
{
  int n=1;
  if(!end)
    end=s1+szlen(s1,0);
  for(;s1<end;s1++) {
    if((*s1=='\n')||(*s1=='\r')) {
      n++;
      if(((s1[1]=='\n')||(s1[1]=='\r'))&&(s1[0]!=s1[1]))
        s1++;
    }
  } // for
  return n;
} // szcountlines()

// JFL 18 Mar 18
int szcpy(chr *dst,int dstSize,cchr *s1,cchr *s1x)
{
  chr *dst0=dst;
  if(!s1||(dstSize<1)) return 0;
  dstSize--; // always leave room for term
  for(;dstSize;dstSize--) {
    if((s1x&&(s1>=s1x))||!*s1)
      break;
    *dst++=*s1++;
  } // for
  *dst=0; // always term
  return dst-dst0;
} // szcpy()

// JFL 17 Mar 18
int szcmp(cchr *s1,cchr *s1x,cchr *s2,cchr *s2x)
{
  chr c1,c2;
  if(!s1 || !s2) return -100;
  for(;;s1++,s2++) {
    if(s1x && (s1>=s1x))
      c1 = 0;
    else
      c1 = *s1;

    if(s2x && (s2>=s2x))
      c2 = 0;
    else
      c2 = *s2;
    
    if(!c1 || !c2) {
      return c1 - c2;
    }
    
    if(c1 == c2)
      continue;
    return c1 - c2;
  } // for
  return -99;
} // szcmp()

// JFL 17 Mar 18
int szicmp(cchr *s1,cchr *s1x,cchr *s2,cchr *s2x)
{
  chr c1,c2;
  if(!s1 || !s2) return -100;
  for(;;s1++,s2++) {
    if(s1x && (s1>=s1x))
      c1 = 0;
    else
      c1 = *s1;

    if(s2x && (s2>=s2x))
      c2 = 0;
    else
      c2 = *s2;
    
    if(!c1 || !c2) {
      return c1 - c2;
    }
    
    if((c1>='A')&&(c1<='Z'))
      c1+='a'-'A';
    if((c2>='A')&&(c2<='Z'))
      c2+='a'-'A';
    if(c1 == c2)
      continue;
    return c1 - c2;
  } // for
  return -99;
} // szicmp()

// JFL 20 Oct 05
// JFL 22 Jan 07; return NUL on failure
cchr* szsub(cchr *sub,cchr *subx,cchr *s2,cchr *s2x)
{ // find first occurrence of sub in string s2
  cchr *ss,*tt;
  chr c,d;

   if(!s2)
      return s2;
lp:
   if(s2x&&(s2>=s2x))
      return 0; // fail
    if(!*s2)
      return 0; // fail

   for(ss=sub,tt=s2;;ss++,tt++)
   {
      if(subx&&(ss>=subx))
         return s2; // success
      if(!*ss)
         return s2; // success
      if(s2x&&(tt>=s2x))
         return 0; // fail
      if(!(c=*tt))
         return 0; // fail
      d=*ss;
      if(c!=d)
         break; // mismatch
   } // for
   s2++;
   goto lp;
} // szsub()

// JFL 20 Oct 05
// JFL 22 Jan 07; return NUL on failure
// JFL 27 Jun 19; isub
cchr* szisub(cchr *sub,cchr *subx,cchr *s2,cchr *s2x)
{ // find first occurrence of sub in string s2
  cchr *ss,*tt;
  chr c,d;

   if(!s2)
      return s2;
lp:
   if(s2x&&(s2>=s2x))
      return 0; // fail
  if(!*s2)
      return 0; // fail

   for(ss=sub,tt=s2;;ss++,tt++)
   {
      if(subx&&(ss>=subx))
         return s2; // success
      if(!*ss)
         return s2; // success
      if(s2x&&(tt>=s2x))
         return 0; // fail
      if(!(c=*tt))
         return 0; // fail
      d=*ss;
      if((c>'A') && (c<'Z'))
        c+='a'-'A';
      if((d>'A') && (d<'Z'))
        d+='a'-'A';      
      if(c!=d)
         break; // mismatch
   } // for
   s2++;
   goto lp;
} // szisub()

// JFL 02 Nov 05
// JFL 01 Feb 07
// JFL 15 Sep 11; start startx
int szstart(cchr *start,cchr *startx,cchr *s2,cchr *s2x)
{ // test if string s2 starts with s1
   chr s,t;

   if(!start || !s2)
      return -98;

   while((!s2x || (s2<s2x)) && (!startx || (start<startx)))
   {
      s=*start++;
      t=*s2++;
      if(!s) // source hit end, match
         return 0;
      if(s!=t) // mismatch
         return s-t;
   } // while

   if(!*start || (startx && (start>=startx)))
      return 0;

   return -99;
} // szstart()

// JFL 02 Jun 19
int szchrreplace(chr *str,chr*strx,chr from,chr to)
{
  if(!str) return 0;
  if(!strx) strx=str+szlen(str,strx);
  for(;str<strx;str++) {
    if(*str!=from) continue;
    *str=to;
  } // for
  return 0;
} // szchrreplace()

// JFL 17 Mar 18
int szmap(cchr **table,cchr *s1,cchr *s1x)
{
    int i;
    for(i=0;*table;i++,table++) {
      if(!szcmp(*table,0,s1,s1x))
        return i;
    } // for
    return -1;
} // szmap()

// JFL 17 Mar 18
int szimap(cchr **table,cchr *s1,cchr *s1x)
{
    int i;
    for(i=0;*table;i++,table++) {
      if(!szicmp(*table,0,s1,s1x))
        return i;
    } // for
    return -1;
} // szimap()

// JFL 18 Mar 18
int szeos(cchr *s1,cchr *s1x)
{
  if(!s1 || (s1x&&(s1>=s1x)))
    return 1;
  return *s1 ? 0 : 1;
} // szeos()

// JFL 22 Sep 19
int szcountchrs(cchr *str,cchr*strx,cchr*count,cchr*countx)
{
  int r;
  int32 i,num=0;
  int32 countlen=szlen(count,countx);
  cchr *s1;
  chr c;
  if(!strx) strx=str+szlen(str,0);
  for(;str<strx;) {
    c=*str++;
    for(s1=count,i=countlen;i>0;i--) {
      if(c==*s1++) {
        num++;
        break;
      }
    } // for
  } // for
  r=num;
//BAIL:
  return r;
} // szcountchrs()

// JFL 15 Aug 04
// JFL 01 Feb 07; sizeof(chr)
// JFL 17 Jun 07; return review
// JFL 18 Dec 10; two string version
int szcat(chr *dst,int n,cchr *s1,cchr *s1x,cchr *s2,cchr *s2x)
{ // concatenate string to existing
   int org;

   if((n<0) || !s1) return 0;
   org=n;

   // skip over current string
   while(n)
   {
      if(!*dst) break;
      dst++;
      n--;
   } // while

   // concatenate first string
   if(s1)
   while(n && (!s1x || (s1<s1x)))
   {
      if(!*s1) break;
      *dst++=*s1++;
      n--;
   } // while

   // concatenate second string
   if(s2)
   while(n && (!s2x || (s2<s2x)))
   {
      if(!*s2) break;
      *dst++=*s2++;
      n--;
   } // while

    *dst=0;

   return org-n;
} // szcat()

static double sztobinDivisors[] = {
1.0/1.0,
1.0/10.0, 
1.0/100.0, 
1.0/1000.0, 
1.0/10000.0, 
1.0/100000.0,
1.0/1000000.0,
1.0/10000000.0,
1.0/100000000.0,
1.0/1000000000.0,
1.0/10000000000.0,
1.0/100000000000.0,
1.0/1000000000000.0,
1.0/10000000000000.0,
1.0/100000000000000.0,
1.0/1000000000000000.0,
1.0/10000000000000000.0,
1.0/100000000000000000.0,
};

static uns64 sztobinPow[] = {
1,
10,
100,
1000,
10000,
100000,
1000000,
10000000,
100000000,
1000000000,
10000000000,
100000000000,
1000000000000,
10000000000000,
100000000000000,
1000000000000000,
10000000000000000,
100000000000000000,
};

// JFL 14 Apr 18
// JFL 06 Nov 18
int sznum(chr *dst,int dstSize,chr fmt,void *nump)
{ // return num chrs added
  int r;
  int32 i1;
  int64 q1;
  chr digits[32],aa;
  int8 d;
  uns8 neg=0,dlen,base,is64=0,isflt=0,fltdot=0;

  if(fmt=='x')
    base = 16,aa = 'a' - 10;
  else if(fmt=='X')
    base = 16, aa = 'A' - 10;
  else if(fmt=='q')
    base = 10, aa = 'a'-10, is64=1;
  else if((fmt=='f')||(fmt=='g')||(fmt=='e'))
    base = 10,aa='a'-10,isflt=1;
  else
    base = 10, aa = 0;
  if(dstSize<1) return 0;

  if(isflt) {
    is64=1;
    flt64 f1 = *((flt64*)nump);
    f1 *= 100;
    fltdot=2;
    if((q1=f1)<0)
      q1=-q1,neg=1;
  } else if(is64) {
    if((q1=*((int64*)nump))<0)
      q1=-q1,neg=1;
  } else {
    if((i1=*((int*)nump))<0)
      i1=-i1,neg=1;
  }

  for(dlen=0;dlen<sizeof(digits);) {

    if(!is64) {
      if(!i1 && dlen)
        break;
      d = i1 % base;
      i1 /= base;
    } else {
      if(!q1 && dlen)
        break;
      d = q1 % base;
      q1 /= base;
    }

    if(d<=9)
      d += '0';
    else if(d<base)
      d += aa;
    else
      d = '?';
    digits[dlen++] = d;
  } // for
  
  r = dstSize;
  if(neg)
    *dst++ = '-',dstSize--;

  while((dstSize>0) && (dlen>fltdot)) 
    *dst++ = digits[--dlen], dstSize--;
  if(dstSize && fltdot) {
    *dst++ = '.',dstSize--;
  while((dstSize>0) && dlen)
    *dst++ = digits[--dlen], dstSize--;
  }

  if(dstSize>0)
    *dst=0;
  else
    dst[-1]=0;
  return r - dstSize;
} // sznum()

// JFL 09 Jun 19
static cchr* sztobin10(cchr *s1,cchr *s1x,int *dstp)
{
  chr c,isneg=0;
  int v=0;
  for(;(c=*s1) && (!s1x||(s1<s1x));s1++) {
    if((c>='0')&&(c<='9'))
      c-='0';
    else if(c=='+')
      continue;
    else if(c=='-') {
      isneg=1;
      continue;
    } else
      break;
    v*=10;
    v+=c;
  } // for
  *dstp=isneg?-v:v;
  return s1;
} // sztobin10()

// JFL 03 May 18
// JFL 06 Nov 18
// JFL 09 Jan 19; added exp for flt64
cchr* sztobin(cchr *s1,cchr *s1x,uns8 base,chr dsttype,void *dstp)
{
  int32 vali=0;
  int64 valq=0;
  cchr *ss,*sx=0,*sdot=0;
  chr c;
  chr cbase;
  uns8 is64;
  uns8 neg=0;
  int exp=0;

  is64 = (dsttype=='q')||(dsttype=='f')||(dsttype=='g');

  if(*s1=='-')
    neg=1,s1++;
  if(*s1=='+')
    s1++;

  if(((s1[0])=='0')&&(((s1[1])=='x')||((s1[1])=='X')))
  {
    s1+=2;
    base=16;
  }
  if(!base)
    base=10;
  cbase = base<=10 ? '0'+base : 'a' + base-10;

  for(ss=s1;(c=*ss) && (!s1x||(ss<s1x));ss++) {
    if((c>='A')&&(c<='Z'))
      c=c-'A'+'a'; // tolower

    if((c>='0')&&(c<='9'))
      c-='0';
    else if((c>='a')&&(c<='z')) {
      if(c<cbase)
        c=c-'a'+10;
      else if(c=='e') {
        sx=sztobin10(ss+1,s1x,&exp);
        break;
      } else {
        break;
      }
    } else if(c=='.') {
      sdot=ss;
      continue;
    } else
      break;
    if(!is64) {
      vali*=base;
      vali+=c;
    } else {
      valq*=base;
      valq+=c;
    }
  } // for
  if(!sx)
    sx=ss; // end used for dot-shifting

  switch(dsttype) {
  default:
  case 'i':
    if(neg)
      vali=-vali;
  case 'u':
    *((int32*)dstp)=vali;
    break;
  case 'q':
    if(neg)
      valq=-valq;
    *((int64*)dstp)=valq;
    break;
  case 'f':
  case 'g': {
    double dval;
    dval = neg  ? -((double)valq) : valq;
    if(sdot||exp) {
      int16 pow=-exp;
      if(sdot)
        pow += ss-sdot-1;
      while(pow<0)
        pow++,dval*=10;
      while(pow>=NUM(sztobinDivisors))
        pow--,dval*=0.1;
      if((pow>0)&&(pow<NUM(sztobinDivisors)))
        dval *= sztobinDivisors[pow];
  }
  if(dsttype!='g')
    *((flt32*)dstp)=dval;
  else
    *((flt64*)dstp)=dval;
  } break;
  } // switch

  return sx;
} // sztobin()

// JFL 10 Apr 18
int szfmt_v(chr *dst,int dstSize,cchr *fmt,va_list args)
{ // minimal, safe string formatter
 // minimal, safe string formatter
  int r = 0;
  int32 i1;
  int64 q1;
  flt64 d1;
  chr *dst0 = dst;
  cchr *s1,*s2;
  chr c;
  
  if(!fmt || !dst || (dstSize<=1))
    return 0;
  
  dstSize--; // reserve for size

  for(;dstSize>0;) {
    if(!(c=*fmt++))
      break;
    if(c!='%') {
      *dst++=c,dstSize--;
      continue;
    }
    if(!(c=*fmt++))
       break;
    switch(c) {
    case 'c':
      if(!(c=va_arg(args,int)))
        break;
      if(c!=C_SZ_SKIP)
        *dst++=c,dstSize--;
      break;
    case 'r':
      if(!(c=*fmt++))
        break;
      i1=va_arg(args,int);
      while((i1>0)&&(dstSize>0)) {
        *dst++=c,dstSize--,i1--;
      } // while
      break;
    case 'S':
      s1=va_arg(args,cchr*);
      s2=va_arg(args,cchr*);
      goto cpy_str;
    case 's':
      s1=va_arg(args,cchr*);
      s2=0;
    cpy_str:
      if(!s1) break;
      while(dstSize>0) {
        if(s2 && (s1>=s2))
            break;
        if(!(*dst=*s1++))
            break;
        dst++,dstSize--;
      } // while
      break;
    case 'p':
      if(dstSize>2)
        dst[0]='0',dst[1]='x',dst+=2,dstSize-=2,c='x';
    case 'd':
    case 'x':
    case 'X':
    num_32:
      i1=va_arg(args,int);
      r=sznum(dst,dstSize,c,&i1);
      dst+=r,dstSize-=r;
      break;
    case 'l':
      if(fmt[0]=='d') {
        fmt++;
        c='d';
        goto num_32;
      } else if((fmt[0]=='l') && (fmt[1]=='d')) {
        fmt+=2;
        c='q';
      }
      // drop through   
    case 'q':
      q1=va_arg(args,int64);
      r=sznum(dst,dstSize,c,&q1);
      dst+=r,dstSize-=r;
      break;
    case 'f': // flt32 (floats passed as flt64)
    case 'g': // flt64
    case 'e': // flt64 -- todo: add sznume64
      d1=va_arg(args,flt64);
      r=sznum(dst,dstSize,c,&d1);
      dst+=r,dstSize-=r;
      break;   
    } // switch
  } // for
  
  *dst = 0;
  r = PTR_DIFF(dst,dst0);
  return r;
} // szfmt_v()

// JFL 10 Apr 18
int szfmt(chr *dst,int dstSize,cchr *fmt,...)
{
  int r;
  va_list args;
  va_start(args,fmt);
  r = szfmt_v(dst,dstSize,fmt,args);
  va_end(args);
  return r;
} // szfmt()

// EOF
