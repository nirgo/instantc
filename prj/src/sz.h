// sz.h
// Copyright (C) 2011-2018 Joe Linhoff, All Rights Reserved.
#ifndef SZ_H
#define SZ_H

#include "base.h"

#ifdef __cplusplus // C in C++ wrapper
PFM_API "C" {       //
#endif             //

///////////////////////////////////////////////////////////////////////////////
// SZ STRINGS

#define C_IS_EOL(c) ((c=='\n')||(c=='\r'))
#define C_IS_WHITE(c) ((c==' ')||(c=='\t')||(c=='\n')||(c=='\r'))
#define C_IS_ID(c) (((c>='A')&&(c<='Z')) \
  || ((c>='a')&&(c<='z')) || ((c>='0')&&(c<='9')) || (c=='_'))
#define C_IS_DIGIT(c) ((c>='0')&&(c<='9'))
#define C_SZ_SKIP (1)
#define C_IS_NUMMOD(c) \
  (((c>='0')&&(c<='9'))||(c=='.')||(c=='-')||(c=='e')||(c=='E'))
#define C_IS_UPPER(_c_) (((_c_)>='A')&&((_c_)<='Z'))

PFM_API cchr* szskipwhite(cchr *s1,cchr *s1x);
PFM_API cchr* sztillwhite(cchr *s1,cchr *s1x);
PFM_API cchr* szskiparg(cchr *str,cchr*strx,cchr **s1p,cchr **s1xp);
PFM_API cchr* sztrimwhite(cchr *s1,cchr *s1x);
PFM_API cchr* szskipeol(cchr *s1,cchr *s1x);
PFM_API cchr* sztilleol(cchr *s1,cchr *s1x);
PFM_API cchr* szskipid(cchr *s1,cchr *s1x);
PFM_API cchr* sztillid(cchr *s1,cchr *s1x);
PFM_API cchr* sztillchr(cchr *s1,cchr *s1x,cchr *till,cchr *tillx);
PFM_API cchr *szlastslash(cchr *s1,cchr *s1x);
PFM_API cchr* szchr(cchr *s1,cchr *s1x,chr match);
PFM_API cchr* szrchr(cchr *s1,cchr *s1x,chr match);
PFM_API int szstart(cchr *start,cchr *startx,cchr *s2,cchr *s2x);
PFM_API int szchrreplace(chr *str,chr*strx,chr from,chr to);
PFM_API int sztolower(chr *str,chr *strx);
PFM_API int szcpy(chr *dst,int dstSize,cchr *s1,cchr *s1x);
PFM_API int szcmp(cchr *s1,cchr *s1x,cchr *s2,cchr *s2x);
PFM_API int szicmp(cchr *s1,cchr *s1x,cchr *s2,cchr *s2x);
PFM_API cchr* szsub(cchr *sub,cchr *subx,cchr *s2,cchr *s2x);
PFM_API cchr* szisub(cchr *sub,cchr *subx,cchr *s2,cchr *s2x);
PFM_API int szcountchrs(cchr *str,cchr*strx,cchr*count,cchr*countx);
PFM_API int szcountlines(cchr *s1,cchr *end);
PFM_API int szcat(chr *dst,int n,cchr *s1,cchr *s1x,cchr *s2,cchr *s2x);
PFM_API int szimap(cchr *table[],cchr *s1,cchr *s1x);
PFM_API int szeos(cchr *s1,cchr *s1x);
PFM_API int szlen(cchr *s1,cchr *s1x);
PFM_API int szsize(cchr *s1,cchr *s1x);
PFM_API int szfmt(chr *dst,int dstSize,cchr *fmt,...);
PFM_API int szfmt_v(chr *dst,int dstSize,cchr *fmt,va_list args);
PFM_API cchr* sztobin(cchr *s1,cchr *s1x,uns8 base,chr dsttype,void *dstp);

PFM_API int sznume64(chr *dst,int dstSize,flt64 *nump);

#ifdef __cplusplus // C in C++ wrapper
} //
#endif //

#endif // SZ_H
