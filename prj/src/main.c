// main.c -- Copyright (C) 2019 Joe Linhoff
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy
// of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required
// by applicable law or agreed to in writing, software distributed under the
// License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
// OF ANY KIND, either express or implied. See the License for the specific
// language governing permissions and limitations under the License.
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include "base.h"
#include "vers.h"
#include "sz.h"

static cchr *MainCoprShort="Copyright (C) 2019 Joe Linhoff - see LICENSE file";

typedef struct {
  int dummy;
} MainGlobals;

////////////////////////////////////////////////////////////////////////////////

MainGlobals MainG;

static chr *extC2 = "c";
static chr *extCPP2 = "cpp";
static chr *extExe2 = "exe";
static cchr *dashSC = "-sc";
static cchr *dashBeta = "-bc";
static cchr *dashLib = "-lc";

static cchr *metaStart = "$(";
static const int metaStartLen = 2;
static cchr *metaEnd = ")";
static const int metaEndLen=1;

static cchr *rcfile=".instantc";

#if BUILD_MINGW32
#define C_DIRSLASH '\\'
#define DIRSLASH "\\"
#else
#define C_DIRSLASH '/'
#define DIRSLASH "/"
#endif

#define EXTCODE_SIZE_MAX (sizeof(extCPP)+1) // of C or CPP ext
#define FILEPATH_BUF_SIZE 2048 // replace with dynamic allocs
#define M_BLOCKID_REF 0x4000

#if BUILD_MINGW32 // used for development
#define DEV_RCFILE "D:\\ainstantc\\prj\\dotinstantc"
#endif // BUILD_MINW32

#define SC_ARGC "_scargc"
#define SC_ARGV "_scargv"
#define SC_MAINRC "_scmainrc"

static cchr *patchCInclude = "<stdio.h>";
static cchr *patchCCInclude = "<iostream>";
static cchr *patchMainHead = "int main(int "SC_ARGC",char *"SC_ARGV"[])\n{\n" \
  "int "SC_MAINRC"=0;\n";
static cchr *patchMainTail = "return "SC_MAINRC";\n}\n";
static cchr *patchBetaInit = "int sc_beta=0;for(char*s="SC_ARGV"[1];*s;)" \
  "sc_beta=(sc_beta*10)+(*s++-'0');\n";
static cchr *patchBetaStart = "if(sc_beta==%d){";
static cchr *patchBetaEnd = "}\n";

// FORWARDS
static int scMetaSet(BaseLink *list,cchr *orgbuf,cchr *str,cchr *strx);

//////////////////////////////////////////////////////

#define METATOKS_X \
  X(NONE) \
  X(ARGC) \
  X(ARGV) \
  X(BETA) \
  X(BLOCK) \
  X(CFLAGS) \
  X(EMIT) \
  X(FINAL) \
  X(GDB) \
  X(INCLUDE) \
  X(INCLUDELIB) \
  X(INIT) \
  X(LANG) \
  X(LFLAGS) \
  X(LIB) \
  X(MAIN) \
  X(MAINRC) \
  X(FUNCTION) \
  X(FUNCTIONLIB) \
  X(FUNCTIONS) \
  X(SEC) \
  X(SECTION) \
  X(SET) \
  X(SHELL) \
  X(SKIP) \
  X(REFID) \
  X(VERS) \
  X(ZMAX)
  
// METASYSTOK_ user can't specify
#define METASYSTOKS_X \
  XX(INCLUDEBLOCK) \
  XX(INCLUDELIBBLOCK) \
  XX(FUNCTIONBLOCK) \
  XX(FUNCTIONLIBBLOCK) \
  XX(BETABLOCK) \
  XX(MAINBLOCK)

#define XX(a) METASYSTOK_##a,
#define X(a) METATOK_##a,
typedef enum { METATOKS_X METASYSTOKS_X METATOK_NUM } metatok_enum;
#undef X
#undef XX

#define X(a) #a,
static cchr *metaTokTable[] = { METATOKS_X 0 };
#undef X

// blocktypes are different blocks in the file
#define BLOCKTYPES_X \
  X(NONE) \
  X(FIRST) \
  X(META) /* process all meta */ \
  X(INCLUDE) \
  X(INCLUDELIB) \
  X(FUNCTIONLIB) \
  X(FUNCTIONMAIN) \
  X(FUNCTIONBETA) \
  X(MAINHEAD) \
  X(MAININIT) \
  X(BETAINIT) \
  X(BETABODY) \
  X(MAINBODY) \
  X(MAINFINAL) \
  X(MAINTAIL) \
  X(LAST)

#define X(a) BLOCKTYPE_##a,
typedef enum { BLOCKTYPES_X } blocktype_enum;
#undef X

#define X(a) #a,
static cchr *blockTypeTable[] = { BLOCKTYPES_X 0 };
#undef X

//////////////////////////////////////////////////////

int main_nobrk = 0;

// JFL 30 Jul 06
int MainLogOutput(chr* buf, uns len)
{
  printf("%s",buf);
  return 0;
} // MainLogOutput()

#define X(a) "["#a"]",
const char *printTypeNames[] = { PRINT_X };
#undef X

// JFL 02 May 18
void MainPrintf(int pf,cchr *fmt,...)
{
  chr buf[1024]; // max print size for one
  va_list args;
  va_start(args,fmt);

  if((pf<0)||(pf>NUM(printTypeNames))) pf = 0;
  int r = 0;
  if(pf)
    r += szfmt(buf,sizeof(buf),"%s",printTypeNames[pf]);
  r += szfmt_v(buf+r,sizeof(buf)-r,fmt,args);
  MainLogOutput(buf,r);

  va_end(args);
} // MainPrintf()

////////////////////////////////////////////////////////////////////////////////

#define OBJTYPES_X \
  X(None) \
  X(Buf) \
  X(Config) \
  X(SZ) \
  X(Mark)

#define X(a) OBJTYPE_##a,
enum { OBJTYPES_X };
#undef X

enum {
  OBJFNC_NONE,
  OBJFNC_INIT,
  OBJFNC_ZAP,
};

////////////////////////////////////////

int ObjNoneFnc(void *o,int op,...) { return -1; }

////////////////////////////////////////

typedef struct {
  BaseLink lnk;
  chr *buf;
  uns32 bufTop;
  uns32 bufMax;
  uns8 flagFree;
} ObjBuf;

// JFL 18 Sep 19
int ObjBufFnc(ObjBuf *o,int op,...)
{
  va_list args;
  va_start(args,op);
  switch(op) {
  case OBJFNC_ZAP:
    if(o->flagFree)
      MemFree(o->buf);
    BaseLinkUnlink(BL(o));
    MemFree(o);
    break;
  } // switch
  va_end(args);
  return 0;
} // ObjBufFnc()

// JFL 18 Sep 19
int ObjBufNew(ObjBuf **out,uns32 maxSize)
{
  int r;
  ObjBuf *o;
  if((r=MemAlloz(&o,sizeof(*o)+maxSize))<0)
    goto BAIL;
  BaseLinkMakeNode(BL(o),OBJTYPE_Buf);
  if((o->bufMax=maxSize))
    o->buf=(void*)(o+1);

  *out=o;
  r=0;
BAIL:
  return r;
} // ObjBufNew()

////////////////////////////////////////

#define M_FILESCREATED_MAINLANG 0x0001
#define M_FILESCREATED_MAINEXE  0x0002
#define M_FILESCREATED_BETALANG 0x0010
#define M_FILESCREATED_BETAEXE  0x0020
#define M_FILESCREATED_LIBLANG  0x0100
#define M_FILESCREATED_LIBO     0x0200
#define M_FILESCREATED_LIBH     0x0400
#define M_FILESCREATED_LIBA     0x0800

#define M_OBJCONFIG_ISCPP    0x01
#define M_OBJCONFIG_DEBUGGER 0x02
#define M_OBJCONFIG_VERBOSE  0x04
#define M_OBJCONFIG_LAUNCH   0x08
typedef struct {
  BaseLink lnk;
  chr *buf;
  chr *name;
  chr *dot;
  uns16 dotToEndSize;
  uns32 bufSize;
  uns32 filesCreated; // M_FILESCREATED_
  uns8 flags;
  int betaPass;
  int libBuild;
  int argc;
  void *argv;
  chr dashExtMain[16];
  chr dashExtBeta[16];
  chr dashExtLib[16];
  chr cFlagStd[16];
  int numMetaTable[METATOK_NUM];
  uns16 nextRefId; // for references between objects
  blocktype_enum curBlockType;

  int debug; // use debug (not a mask-bit) to allow values
  int verbose;

  cchr *cpath,*cpathx;
  cchr *ccpath,*ccpathx;
  cchr *arpath,*arpathx;
  cchr *dbpath,*dbpathx;
  cchr *sclib,*sclibx;
  cchr *sclibrel,*sclibrelx;
  cchr *cflags,*cflagsx;
  cchr *ccflags,*ccflagsx;
  cchr *ldcflags,*ldcflagsx;
  cchr *ldccflags,*ldccflagsx;
  cchr *arflags,*arflagsx;
  cchr *cstd,*cstdx;
  cchr *ccstd,*ccstdx;
  cchr *scdoc,*scdocx;
  cchr *dbarg,*dbargx;
  cchr *dbenv,*dbenvx;

} ObjConfig;

// JFL 18 Sep 19
int ObjConfigFnc(ObjConfig *o,int op,...)
{
  va_list args;
  va_start(args,op);
  switch(op) {
  case OBJFNC_ZAP:
    BaseLinkUnlink(BL(o));
    MemFree(o);
    break;
  } // switch
  va_end(args);
  return 0;
} // ObjConfigFnc()

// JFL 18 Sep 19
int ObjConfigNew(ObjConfig **out,uns32 bufSize)
{
  int r;
  ObjConfig *o;
  if((r=MemAlloz(&o,sizeof(*o)+bufSize))<0)
    goto BAIL;
  BaseLinkMakeNode(BL(o),OBJTYPE_Config);
  if((o->bufSize=bufSize))
    o->buf=(void*)(1+o);

  *out=o;
  r=0;
BAIL:
  return r;
} // ObjConfigNew()

////////////////////////////////////////

typedef struct {
  cchr *str;
  cchr *strx;
  uns16 refId;
  uns32 fileOff;
} ObjSZOne;

typedef struct {
  BaseLink lnk;
  metatok_enum metaType; // METATOK_
  ObjSZOne *sz;
  chr *buf;
  uns32 szNum;
  uns32 szMax;
  uns32 bufSize;
} ObjSZ;

// JFL 29 Sep 19
int ObjSZFnc(ObjSZ *o,int op,...)
{
  va_list args;
  va_start(args,op);
  switch(op) {
  case OBJFNC_ZAP:
    BaseLinkUnlink(BL(o));
    MemFree(o);
    break;
  } // switch
  va_end(args);
  return 0;
} // ObjSZ()

// JFL 18 Sep 19
int ObjSZNew(ObjSZ **out,uns32 num,uns32 bufSize)
{
  int r;
  ObjSZ *o;

  bufSize=ALIGNMACH(bufSize);
  if((r=MemAlloz(&o,sizeof(*o)+num*sizeof(*o->sz)+bufSize))<0)
    goto BAIL;
  BaseLinkMakeNode(BL(o),OBJTYPE_SZ);
  if((o->szMax=num))
    o->sz=(void*)(1+o);
  if((o->bufSize=bufSize))
    o->buf=PTR_ADD(o,sizeof(*o)+num*sizeof(*o->sz));

  *out=o;
  r=0;
BAIL:
  return r;
} // ObjSZNew()

////////////////////////////////////////

typedef struct {
  BaseLink lnk;
  uns16 mType1;
  uns16 mType2;
  uns32 fhOff;
} ObjMark;

// JFL 13 Oct 19
int ObjMarkFnc(ObjMark *o,int op,...)
{
  va_list args;
  va_start(args,op);
  switch(op) {
  case OBJFNC_ZAP:
    BaseLinkUnlink(BL(o));
    MemFree(o);
    break;
  } // switch
  va_end(args);
  return 0;
} // ObjMarkFnc()

// JFL 13 Oct 19
int ObjMarkNew(ObjMark **out,uns16 mtype1,uns16 mtype2)
{
  int r;
  ObjMark *o;
  if((r=MemAlloz(&o,sizeof(*o)))<0)
    goto BAIL;
  BaseLinkMakeNode(BL(o),OBJTYPE_Mark);
  o->mType1=mtype1;
  o->mType2=mtype2;

  *out=o;
  r=0;
BAIL:
  return r;
} // ObjMarkNew()

////////////////////////////////////////

// JFL 09 Jun 19
void ObjListZap(BaseLink *list)
{
  BaseLink *lnk;
  while((lnk=list->n)&&lnk->t) {
    switch(lnk->t) {
    default:
      PRINT_SYS_ERR("BaseListZap %d not handled\n",lnk->t);
      BaseLinkUnlink(lnk);
      MemFree(lnk);
      break;
    #define X(a) case OBJTYPE_##a: Obj##a##Fnc((void*)lnk,OBJFNC_ZAP);break;
    OBJTYPES_X
    #undef X
    } // switch
  } // while
} // ObjListZap()

////////////////////////////////////////////////////////////////////////////////

// JFL 13 Oct 19
static int scAddMarks(BaseLink *list)
{
  int r,i;
  ObjMark *omark;
  int const marks[] = {
    BLOCKTYPE_FIRST,BLOCKTYPE_META,
    BLOCKTYPE_INCLUDE,BLOCKTYPE_INCLUDELIB,
    BLOCKTYPE_FUNCTIONLIB,BLOCKTYPE_FUNCTIONMAIN,BLOCKTYPE_FUNCTIONBETA,
    BLOCKTYPE_MAINHEAD,BLOCKTYPE_MAININIT,
    BLOCKTYPE_BETAINIT,BLOCKTYPE_BETABODY,
    BLOCKTYPE_MAINBODY,BLOCKTYPE_MAINFINAL,
    BLOCKTYPE_MAINTAIL,BLOCKTYPE_LAST};

  for(i=0;i<NUM(marks);i++) {
    if((r=ObjMarkNew(&omark,marks[i],0))<0)
      goto BAIL;
    BaseLinkBefore(list,BL(omark));
  } // for

  r=0;
BAIL:
  return r;
} // scAddMarks()

////////////////////////////////////////////////////////////////////////////////

// JFL 13 Oct 19
BaseLink* scBlockNext(BaseLink *lnk,uns16 mtype1,uns16 mtype2)
{
  ObjMark *omark;
  for(lnk=lnk->n;lnk->t;lnk=lnk->n) {
      if(lnk->t!=OBJTYPE_Mark) continue;
      omark=(ObjMark*)lnk;
      if(mtype1 && (mtype1!=omark->mType1)) continue;
      if(mtype2 && (mtype2!=omark->mType2)) continue;
      return lnk;
  } // for
  return 0;
} // scBlockNext()

// JFL 13 Oct 19
BaseLink* scBlockFind(BaseLink *list,uns16 mtype1,uns16 mtype2,int flags)
{
  BaseLink *lnk;
  ObjMark *omark;
  for(lnk=list->n;lnk->t;lnk=lnk->n) {
      if(lnk->t!=OBJTYPE_Mark) continue;
      omark=(ObjMark*)lnk;
      if(mtype1 && (mtype1!=omark->mType1)) continue;
      if(mtype2 && (mtype2!=omark->mType2)) continue;
      goto found;
  } // for
  lnk=0;
  goto done;
found:
  if(flags) { // find-next
    // next block - any type
    lnk=scBlockNext(lnk,0,0);
  } // find-next

done:
  return lnk;
} // scBlockFind()
  
////////////////////////////////////////////////////////////////////////////////

// JFL 16 Sep 19
static ObjConfig* scGetConfig(BaseLink *list)
{
  BaseLink *lnk;
  for(lnk=list->n;lnk->t;lnk=lnk->n) {
    if(lnk->t==OBJTYPE_Config)
      return (ObjConfig*)lnk;
  } // for
  return 0;
} // scGetConfig()

typedef enum {
  FORMPATH_NONE,
  FORMPATH_MAINLANG,
  FORMPATH_LIBH,
  FORMPATH_LIBLANG,
  FORMPATH_LIBO,
  FORMPATH_LIBA,
  FORMPATH_BETAEXE,
  FORMPATH_BETALANG,
} formpath_t;

// JFL 19 Nov 19
static int scFormPath(ObjConfig *oconfig,formpath_t formpath)
{
  int r;
  cchr *ext,*dash;
  switch(formpath) {
  default:
    bret(-1);
  case FORMPATH_MAINLANG:
    ext = (oconfig->flags&M_OBJCONFIG_ISCPP)?extCPP2:extC2;
    dash = oconfig->betaPass?oconfig->dashExtBeta:oconfig->dashExtMain;
    r=szfmt(oconfig->dot,oconfig->dotToEndSize,"%s.%s",dash,ext);
    break;
  case FORMPATH_LIBH:
    r=szfmt(oconfig->dot,oconfig->dotToEndSize,".h");
    break;
  case FORMPATH_LIBO:
    r=szfmt(oconfig->dot,oconfig->dotToEndSize,".o");
    break;
  case FORMPATH_LIBA:
    r=szfmt(oconfig->dot,oconfig->dotToEndSize,".a");
    break;
  case FORMPATH_LIBLANG:
    ext = (oconfig->flags&M_OBJCONFIG_ISCPP)?extCPP2:extC2;
    dash = oconfig->dashExtLib;
    r=szfmt(oconfig->dot,oconfig->dotToEndSize,"%s.%s",dash,ext);
    break;
  case FORMPATH_BETAEXE:
    r=szfmt(oconfig->dot,oconfig->dotToEndSize,"%s.%s",
      oconfig->dashExtBeta,extExe2);
    break;
  case FORMPATH_BETALANG:
    ext = (oconfig->flags&M_OBJCONFIG_ISCPP)?extCPP2:extC2;
    r=szfmt(oconfig->dot,oconfig->dotToEndSize,"%s.%s",
      oconfig->dashExtBeta,ext);
    break;
  } // switch

  // r is len
BAIL:
  return r;
} // scFormPath()

// JFL 05 Oct 19
static int scHandleMeta(ObjConfig *oconfig,BaseLink *list,cchr *bufStart,
  cchr *str,cchr *strx)
{ // return metaTok
  int r;
  BaseLink *lnk;
  metatok_enum tok;
  blocktype_enum blocktype;
  cchr *s1,*s2;
  ObjSZ *osz;

  s1=szskipwhite(str,strx);
  str=szskipid(s1,strx);
  tok=szimap(metaTokTable,s1,str);
  s2=szskipwhite(str,strx);
  switch(tok) {
  default:
    PRINT_SYS_ERR("scHandleMeta line:%d tok-not-handled: '%S'\n",
      szcountlines(bufStart,str),s1,str);
    BRK();
    break;
  case METATOK_SET:
  case METATOK_LANG:
    if((r=scMetaSet(list,bufStart,s2,strx))<0)
      goto BAIL;
    break;

  // change code section
  case METATOK_BLOCK:
    PRINT_SYS_ERR("'block' depricatedd, use 'section'\n");
  case METATOK_SEC:
  case METATOK_SECTION:
    s1=sztillwhite(s2,strx);
    tok=szimap(metaTokTable,s2,s1);
    switch(tok) {
    default:
      PRINT_SYS_ERR("unknown section type '%S'\n",s2,s1);
      ret(-1);
    case METATOK_INCLUDELIB:
      oconfig->libBuild=1;
      tok=METASYSTOK_INCLUDELIBBLOCK;
      break;
    case METATOK_LIB:
    case METATOK_FUNCTIONLIB:
      oconfig->libBuild=1;
      tok=METASYSTOK_FUNCTIONLIBBLOCK;
      break;
    case METATOK_FUNCTION:
    case METATOK_FUNCTIONS:
      tok=METASYSTOK_FUNCTIONBLOCK;
      break;
    case METATOK_MAIN:
      tok=METASYSTOK_MAINBLOCK;
      break;
    case METATOK_INCLUDE:
      tok=METASYSTOK_INCLUDEBLOCK;
      break;
    case METATOK_BETA:
      tok=METASYSTOK_BETABLOCK;
      break;
    } // switch
    // tok set
    break;
  case METATOK_FUNCTION:
  case METATOK_FUNCTIONS:
    tok=METASYSTOK_FUNCTIONBLOCK;
    break;
  case METATOK_MAIN:
    tok=METASYSTOK_MAINBLOCK;
    break;
  include_block:
    tok=METASYSTOK_INCLUDEBLOCK;
    break;
  beta_block:
    tok=METASYSTOK_BETABLOCK;
    break;
    
  // include files and libraries
  case METATOK_INCLUDE:
    if(s2==strx) // treat as block-marker with empty meta-sub
      goto include_block;
  case METATOK_LIB:

    if(!(lnk=scBlockFind(list,BLOCKTYPE_INCLUDE,0,1)))
      bret(-75);

    if((r=ObjSZNew(&osz,1,0))<0)
      goto BAIL;
    BaseLinkBefore(lnk,BL(osz));

    osz->metaType=tok;
    osz->szNum=1;
    osz->sz[0].str=s2,osz->sz[0].strx=strx;

    break;
    
  // process during meta pass
  case METATOK_BETA:
    if(s2==strx) // treat as block-marker with empty meta-sub
      goto beta_block;
    // think we need another block
    blocktype=BLOCKTYPE_BETABODY;
    goto link_into_block;
  case METATOK_EMIT:
    r=0;
  case METATOK_SHELL:
  case METATOK_GDB:
  case METATOK_ARGC:
  case METATOK_ARGV:
  case METATOK_MAINRC:
  case METATOK_VERS:
  case METATOK_CFLAGS:
  case METATOK_LFLAGS:
    blocktype=BLOCKTYPE_META;
    link_into_block:

    // link all substitution data into the meta block
    if(!(lnk=scBlockFind(list,blocktype,0,1)))
      bret(-75);

    if((r=ObjSZNew(&osz,1,0))<0)
      goto BAIL;
    BaseLinkBefore(lnk,BL(osz));

    osz->metaType=tok;
    osz->szNum=1;
    osz->sz[0].str=s2,osz->sz[0].strx=strx;
    osz->sz[0].refId=++oconfig->nextRefId;
    tok=METATOK_REFID;

    break;
  } // switch

  r=tok;
BAIL:
  if(r<0)
    tok=METATOK_NONE;
  return r;
} // scHandleMeta()

// JFL 10 Oct 19
static int scMeta(cchr *str,cchr *strx,cchr **sp,cchr **mp,cchr **ap,cchr **ep,cchr **np)
{ // return meta-token id
  // the quote is optional, can be any non-id character, must match at end
  // $("meta arg  ")   --
  // |  |    |    | \  --
  // s  m    a    e  n -- s:start m:meta a:arg e:end n:next
  cchr *s1=szsub(metaStart,metaStart+metaStartLen,str,strx);
  if(!s1)
    return -1; // return <0 if not found
  if(sp)
    *sp=s1;
  
  chr match[metaEndLen+3],*mm=match;
  s1+=metaStartLen;
  if(!C_IS_ID(*s1))
    *mm++=*s1++;
  szcpy(mm,sizeof(match),metaEnd,metaEnd+metaEndLen);

  if(mp)
    *mp=s1;
  
  cchr *s2 = szskipid(s1,strx);
  int tok=szimap(metaTokTable,s1,s2);
  if(tok<0) // not-found is OK -- not an error
    tok=METATOK_NONE;

  s2=szskipwhite(s2,strx);
  if(ap)
    *ap=s2;

  if(!(s1=szsub(match,0,s2,strx))) {
    tok=-2;
    goto BAIL; // return <0 if incomplete
  }

  if(ep)
    *ep=s1;
  
  s1 += szlen(match,0);
  if(np)
    *np=s1;

BAIL:
  return tok;
} // scMeta()

// JFL 13 Oct 19
static int scGetLastSZ(BaseLink *list,ObjSZ **out,
  uns16 markType,uns16 metaType,uns16 createNeedNum)
{
  int r;
  ObjSZ *osz,*oszlast=0;
  ObjMark *omark;
  BaseLink *lnk,*lnkbefore=0;

  // look for mark type
  for(lnk=list->n;lnk->t;lnk=lnk->n) {
    if(lnk->t!=OBJTYPE_Mark)
      continue;
    omark=(ObjMark*)lnk;
    if(omark->mType1!=markType)
      continue;
    goto found_mark1;
  } // for
  bret(-1);
found_mark1:
 
  // find last SZ in this block
  for(lnk=lnk->n;lnk->t;lnk=lnk->n) {
    if(lnk->t==OBJTYPE_Mark) {
      lnkbefore=lnk;
      break; // mark to end block
    }
    if(lnk->t!=OBJTYPE_SZ)
      continue;
    osz=(ObjSZ*)lnk;
    if(metaType && (metaType != osz->metaType))
      continue;
    oszlast=osz;
  } // for
  
  if(oszlast && ((oszlast->szMax-oszlast->szNum)>=createNeedNum)) {
    if(out)
      *out=oszlast;
    ret(0);
  }
    
  if(!createNeedNum || !lnkbefore)
    ret(1); // not created

  // create a few more than asked for
  if(createNeedNum<8)
    createNeedNum+=4;
  else
    createNeedNum+=createNeedNum>>1;
    
  if((r=ObjSZNew(&osz,createNeedNum,0))<0)
    goto BAIL;
  BaseLinkBefore(lnkbefore,BL(osz));
  osz->metaType=metaType;

  if(out)
    *out=osz;
      
  r=0;
BAIL:
  return r;
} // scGetLastSZ()
  
// JFL 16 Sep 19
static int scScan(BaseLink *list,cchr *scpath,cchr *scpathx)
{
  int r,s,nsubneed;
  metatok_enum tok;
  blocktype_enum blocktype;
  ObjBuf *obuf;
  ObjSZ *osz;
  ObjSZOne *sz;
  chr *srcalloc=0;
  cchr *s1,*s2,*buf,*bufx,*eol,*eos;
  ObjConfig *oconfig;

  if(!(oconfig=scGetConfig(list)))
    bret(-1);

  //
  // READ FILE
  //

  if((r=BaseReadFile(scpath,scpathx,&srcalloc,0))<0)
    goto BAIL;
  s=r; // size
  if((r=ObjBufNew(&obuf,0))<0)
    goto BAIL;
  BaseLinkBefore(list,BL(obuf));
  obuf->buf=srcalloc,srcalloc=0; // pass ownership
  obuf->flagFree=1; // request to be freed
  buf=obuf->buf,bufx=obuf->buf+s;

  //
  // COUNT MAX SUBSTITUTIONS
  //

  nsubneed = 4;
  eol = buf;
  while((s1 = szsub(metaStart,0,eol,bufx)))
    eol = s1+2, nsubneed++;

  //
  //
  //

  blocktype=BLOCKTYPE_MAINBODY;
  if((r=scGetLastSZ(list,&osz,blocktype,0,nsubneed))<0)
    goto BAIL;
  sz=0;
  
  //
  //
  //

  s1=szskipwhite(buf,bufx);
  if((*s1=='#')&&(s1[1]=='!')) {
    // skip first line if it starts with #!
    s1=sztilleol(buf,bufx);
    eol=szskipeol(s1,bufx);
  } else {
    eol=s1;
  }

  for(;;) {
    buf=szskipwhite(eol,bufx); // skips white (& eol)
    if(szeos(buf,bufx))
      break; // done
    eol=sztilleol(buf,bufx); // end-of-line
    s1=szsub("//",0,buf,eol); // todo: handle false-positives
    eos=s1?s1:eol; // end-of-string
    if(szeos(buf,eos))
      continue; // skip whole line
  
    // start a new string -- we may break this up later
    if(!sz) {
      if(osz->szNum>=osz->szMax)
        bret(-44);
      sz=osz->sz+osz->szNum; // bump up when we end the string
      sz->str=buf,sz->strx=bufx;
    }
    
    // look for well-formed meta-substitution
    cchr *metastart=0,*metaend;
    if((s1=szsub(metaStart,0,buf,eos))) { // look for meta-sub
      scMeta(s1,eos,&s1,&metastart,0,&metaend,&s2);
      // $(...)
      // |     |
      // s1    s2 (next)
    } // look for meta-sub
    
    if(metastart) { // meta-sub
    
      //
      // END CURRENT STRING
      //
      
      // end current string when we start a substitution
      if(sz->str!=s1) { // skip empty string
        sz->strx=s1;
        osz->szNum++;
        sz=0;
        nsubneed--;
      } // end current
      
      //
      // HANDLE META-SUBSTITUTION
      //

      // handle substitution
      oconfig->curBlockType=blocktype;
      if((r=scHandleMeta(oconfig,list,obuf->buf,metastart,metaend))<0)
        goto BAIL;
      tok=r;
      
      // meta-sub on end of line or meta-sub meta-sub -- skip whitespace
      if((s1=szskipwhite(s2,eol))==eol)
        s2=szskipwhite(eol,bufx);
      else if(!szstart(metaStart,0,s1,0))
        s2=s1;

      // (possibly) switch block
      switch(tok) {
      default: r=blocktype; break;
      case METASYSTOK_INCLUDEBLOCK: r=BLOCKTYPE_INCLUDE; break;
      case METASYSTOK_INCLUDELIBBLOCK: r=BLOCKTYPE_INCLUDELIB; break;
      case METASYSTOK_FUNCTIONLIBBLOCK: r=BLOCKTYPE_FUNCTIONLIB; break;
      case METASYSTOK_FUNCTIONBLOCK: r=BLOCKTYPE_FUNCTIONMAIN; break;
      case METASYSTOK_BETABLOCK: r=BLOCKTYPE_FUNCTIONBETA; break;
      case METASYSTOK_MAINBLOCK: r=BLOCKTYPE_MAINBODY; break;
      } // switch

      if(blocktype!=r) {
        blocktype=r;
        if((r=scGetLastSZ(list,&osz,blocktype,0,nsubneed))<0)
          goto BAIL;
        sz=0;
      } // switch to block

      // start a new string -- we may break this up later
      if(!sz) {
        if(osz->szNum>=osz->szMax)
          bret(-44);
        sz=osz->sz+osz->szNum; // bump up when we end the string
      }

      //
      // START OF NEW STRING
      //

      // start of next string == end of meta-sub
      if(tok == METATOK_REFID) {
        // 'use' this sz, setup for next
        sz->str=s2,sz->strx=s2; 
        sz->refId=oconfig->nextRefId | M_BLOCKID_REF; // depends on pre-inc
        if(osz->szNum>=osz->szMax)
          bret(-44);
        sz=osz->sz+(++osz->szNum); // bump up
      }
      sz->str=s2,sz->strx=bufx; 

      eol=s2;
    } // meta-sub
  } // for

  // end current string if there is one
  if(sz) {
    sz->strx=s1;
    osz->szNum++;
  }

  // count
  BaseLink *lnk;
  for(lnk=list->n;lnk->t;lnk=lnk->n) {
    if(lnk->t!=OBJTYPE_SZ) continue;
    osz=(ObjSZ*)lnk;
    if((osz->metaType>=0) && (osz->metaType<NUM(oconfig->numMetaTable)))
      oconfig->numMetaTable[osz->metaType]++;
  } // for
  
  r=0;
BAIL:
  if(srcalloc)
    MemFree(srcalloc);
  return r;
} // scScan()

// JFL 10 Oct 19
static int scPatchLib(BaseLink *list,ObjSZOne *sz)
{
  int r,s;
  metatok_enum tok;
  blocktype_enum blocktype;
  chr buf[FILEPATH_BUF_SIZE];
  chr *filebuf=0,*filebufx;
  cchr *str,*a,*e,*n;
  ObjSZ *osz;
  ObjConfig *oconfig;
  BaseLink *lnk;
  
  if(!(oconfig=scGetConfig(list)))
    bret(-99);
  
  szfmt(buf,sizeof(buf),"%S%S.h",oconfig->sclib,oconfig->sclibx,sz->str,sz->strx);
  if((r=BaseReadFile(buf,0,&filebuf,0))<0)
    bret(-77);
  filebufx=filebuf+r;
  
  for(str=filebuf;;) {
    
    // cchr **sp,cchr **mp,cchr **ap,cchr **ep,cchr **np)
    if((r=scMeta(str,filebufx,0,0,&a,&e,&n))<0)
      break;
    str=n;
    tok=r;

    switch(tok) {
    default:
      bret(-1);
    case METATOK_INIT:
      blocktype=BLOCKTYPE_MAININIT;
      // drop through
      if(0) {
    case METATOK_FINAL:
      blocktype=BLOCKTYPE_MAINFINAL;
      }
      s=szsize(a,e)+2; // add room for eol
      if((r=ObjSZNew(&osz,1,s))<0)
        goto BAIL;
      
      if(!(lnk=scBlockFind(list,blocktype,0,1)))
        bret(-99);
      BaseLinkBefore(lnk,BL(osz));
      
      r=szcpy(osz->buf,osz->bufSize,a,e);
      osz->szNum=1;
      osz->sz[0].str=osz->buf;

      if(!C_IS_EOL(osz->buf[r])) {
        szcpy(osz->buf+r,osz->bufSize-r,"\n",0);
      }
  
      break;
    } // switch
  } // for

  r=0;
BAIL:
 if(filebuf)
    MemFree(filebuf);
  return r;
} // scPatchLib()

// JFL 29 Sep 19
static int scPatch(BaseLink *list)
{
  int r,i;
  blocktype_enum blocktype;
  ObjSZ *osz;
  ObjSZOne *sz;
  BaseLink *lnk;
  ObjConfig* oconfig;
  
  if(!(oconfig=scGetConfig(list)))
    bret(-99);

  //
  // FIND LIBRARY INIT & FINAL
  //

  for(lnk=list->n;lnk->t;lnk=lnk->n) {
    if(lnk->t!=OBJTYPE_SZ) continue;
    osz=(ObjSZ*)lnk;
    if(osz->metaType!=METATOK_LIB) continue;
    for(sz=osz->sz,i=0;i<osz->szNum;i++,sz++) {
      if((r=scPatchLib(list,sz))<0)
        goto BAIL;
    } // for

  } // for

  //
  // HEADERS
  //
  
  // add default include if no lib, no other includes
  if(!oconfig->numMetaTable[METATOK_INCLUDE]
    && !oconfig->numMetaTable[METATOK_LIB]) {
    blocktype=BLOCKTYPE_INCLUDE;
    if(!(lnk=scBlockFind(list,blocktype,0,1)))
      bret(-30);
      
    if((r=ObjSZNew(&osz,1,0))<0)
      goto BAIL;
    BaseLinkBefore(lnk,BL(osz));

    osz->metaType=METATOK_INCLUDE;

    if(!(oconfig->flags&M_OBJCONFIG_ISCPP))
      osz->sz[0].str=patchCInclude;
    else
      osz->sz[0].str=patchCCInclude;
    osz->szNum=1;
  }

  // add main header
  if(!(lnk=scBlockFind(list,BLOCKTYPE_MAINHEAD,0,1)))
    bret(-31);
  if((r=ObjSZNew(&osz,1,0))<0)
    goto BAIL;
  BaseLinkBefore(lnk,BL(osz));
  osz->sz[0].str=patchMainHead;
  osz->szNum=1;
  
  if(oconfig->betaPass) {
    if(!(lnk=scBlockFind(list,BLOCKTYPE_BETAINIT,0,1)))
      bret(-31);
    if((r=ObjSZNew(&osz,1,0))<0)
      goto BAIL;
    BaseLinkBefore(lnk,BL(osz));
    osz->sz[0].str=patchBetaInit;
    osz->szNum=1;    
  }

  //
  // FOOTERS
  //
  
  // add main final
  if(!(lnk=scBlockFind(list,BLOCKTYPE_MAINTAIL,0,1)))
    bret(-31);

  if((r=ObjSZNew(&osz,1,0))<0)
    goto BAIL;
  BaseLinkBefore(lnk,BL(osz));

  osz->sz[0].str=patchMainTail;
  osz->szNum=1;
  
  r=0;
BAIL:
  return r;
} // scPatch()

// JFL 09 Oct 19
static int scFindSZRef(ObjSZOne const **outp,BaseLink *list,uns16 metaType,uns16 refId)
{
  BaseLink *lnk;
  ObjSZ *osz;
  ObjSZOne *sz,*szx;

  for(lnk=list->n;lnk->t;lnk=lnk->n) {
    if(lnk->t!=OBJTYPE_SZ)
      continue;
    osz=(ObjSZ*)lnk;
    if(metaType && (osz->metaType!=metaType))
      continue;
    sz=osz->sz;
    szx=sz+osz->szNum;
    for(;sz!=szx;sz++) {
      if(sz->refId==refId) {
        if(outp)
          *outp=sz;
        return osz->metaType;
      }
    } // for
  } // for
  return -1;
} // scFindSZRef()

// JFL 10 Oct 19
static int scCleanResponse(chr *str,chr *strx,int flags)
{
  if(flags)
    goto done;
    
  if(!strx)
    strx=str+szlen(str,0);
  if((strx>str) && (C_IS_EOL(strx[-1]))) {
    strx--;
    if(C_IS_EOL(strx[-1])&&(strx[-1]!=strx[0]))
      strx--;
  }
  
done:
  return szlen(str,strx);
} // scCleanResponse()

// JFL 09 Oct 19
static int scWriteSubstitution(BaseLink *list,ObjSZ *osz,ObjSZOne *sz,FILE *fh,ObjConfig *oconfig)
{ // return non-zero to not-write the sz itself
  int r;
  metatok_enum metaType;
  ObjSZOne const *sz2;
  chr buf[2048];
  chr *resp=0,*rs=0,*rsx;

  if(sz->refId) {
    if((r=scFindSZRef(&sz2,list,0,sz->refId&~M_BLOCKID_REF))<0)
      bret(1);
    metaType=r;
  } else if(osz) {
    metaType=osz->metaType;
  } else {
    metaType=0;
  }
  
  switch(metaType) {
  default:
    PRINT_SYS_ERR("scWriteSubstitution unhandled type:%d\n",metaType);
    bret(1);
  case METASYSTOK_FUNCTIONLIBBLOCK:
    ret(0);
  case METATOK_CFLAGS:
  case METATOK_LFLAGS:
    break;
  case METATOK_GDB:
    sz->fileOff=ftell(fh);
    ret(0);
  case METATOK_SHELL:
    if((r=BaseRunScript(&resp,sz2->str,sz2->strx,0))<0) {
      r=szfmt(buf,sizeof(buf),"/* SHELL '%S' */",sz2->str,sz2->strx);
      rs=buf,rsx=0;
    } else {
      r=scCleanResponse(resp,0,0);
      rs=resp,rsx=rs+r;
    }
    break;
  case METATOK_BETA:
    if(oconfig->betaPass)
      break;
    if(!(sz->refId&M_BLOCKID_REF))
      break;
    if((r=scFormPath(oconfig,FORMPATH_BETAEXE))<0)
      goto BAIL;
    szfmt(oconfig->dot+r,oconfig->dotToEndSize-r," %d",
      sz->refId&~M_BLOCKID_REF);
    PRINT_SYS_VRB("B> %s\n",oconfig->buf);
    if((r=BaseRunScript(&resp,oconfig->buf,0,0))<0) {
      r=szfmt(buf,sizeof(buf),"/* BETA '%s' */",oconfig->buf);
      rs=buf,rsx=0;
    } else {
      r=scCleanResponse(resp,0,1);
      rs=resp,rsx=rs+r;
    }
    break;
  
  case METATOK_EMIT:
    r=szfmt(buf,sizeof(buf),"%S",sz2->str,sz2->strx);
    rs=buf,rsx=0;
    break;
  case METATOK_ARGV:
    r=szfmt(buf,sizeof(buf),""SC_ARGV);
    rs=buf,rsx=0;
    break;
  case METATOK_ARGC:
    r=szfmt(buf,sizeof(buf),""SC_ARGC);
    rs=buf,rsx=0;
    break;
  case METATOK_MAINRC:
    r=szfmt(buf,sizeof(buf),""SC_MAINRC);
    rs=buf,rsx=0;
    break;
  case METATOK_VERS:
    r=szfmt(buf,sizeof(buf),"%s",MAINVERS_SZ);
    rs=buf,rsx=0;
    break;
  case METATOK_INCLUDE:
    r=szfmt(buf,sizeof(buf),"#include %S\n",sz->str,sz->strx);
    rs=buf,rsx=0;
    break;
  case METATOK_LIB:
    r=szfmt(buf,sizeof(buf),"#include \"%S%S.h\"\n",
      oconfig->sclibrel,oconfig->sclibrelx,sz->str,sz->strx);
    rs=buf,rsx=0;
    break;
  } // switch

  if((r=szlen(rs,rsx))>0) {
    if(1!=fwrite(rs,r,1,fh))
      bret(-19);
  }

  r=metaType;
BAIL:
  if(resp)
    MemFree(resp);
  return r;
} // scWriteSubstitution()

// JFL 16 Nov 19
static int scLibPassFileMark(BaseLink *list,ObjConfig *oconfig,
  BaseLink *lnk,FILE *fh)
{
  int r;
  ObjMark *omark;
  
  if(lnk->t!=OBJTYPE_Mark)
    ret(1);
  omark=(ObjMark*)lnk;
  omark->fhOff=ftell(fh);

  r=0;
BAIL:
  return r;
} // scLibPassFileMark()

// JFL 16 Sep 19
// JFL 26 Oct 19
static int scWriteCode(BaseLink *list)
{
  int r,i,skip=0;
  //cchr *ext,*dash;
  ObjConfig *oconfig;
  ObjSZ *osz;
  ObjSZOne *sz;
  FILE *fh=0;
  BaseLink *lnk;
  uns8 flagbeta=0;

  if(!(oconfig=scGetConfig(list)))
    bret(-1);
  
  //
  // CREATE C or C++ FILE
  //

  if((r=scFormPath(oconfig,FORMPATH_MAINLANG))<0)
    goto BAIL;
  if(!(fh=fopen(oconfig->buf,"wb")))
    bret(-10);
  oconfig->filesCreated|=M_FILESCREATED_MAINLANG;

  //
  // WRITE ALL IN ORDER
  //

  for(lnk=list->n;lnk->t;lnk=lnk->n) {

    //
    // IDENTIFY BLOCKS
    //

    if(lnk->t==OBJTYPE_Mark) { // show blocks
      chr smbuf[64];
      ObjMark *omark=(ObjMark*)lnk;
  
      if(oconfig->betaPass) {
        // beta-pass
        if(omark->mType1 == BLOCKTYPE_MAINBODY)
          skip=1; // skip main on beta-pass
        else
          skip=0;
      } else {
        // main-pass
        if((omark->mType1 == BLOCKTYPE_FUNCTIONBETA)
          ||(omark->mType1 == BLOCKTYPE_BETAINIT)
          ||(omark->mType1 == BLOCKTYPE_BETABODY))
          skip=1; // skip beta on main-pass
        else
          skip=0;
        
        if(oconfig->libBuild) {
          if((r=scLibPassFileMark(list,oconfig,lnk,fh))<0)
            goto BAIL;
        }
      } // main-pass

      if(oconfig->flags&M_OBJCONFIG_VERBOSE) {
        cchr *s2=skip?"skipping ":"";
        cchr *s1 = (omark->mType1<NUM(blockTypeTable)) 
          ? blockTypeTable[omark->mType1]:"unk";
        r=szfmt(smbuf,sizeof(smbuf),"/* %s%s */\n",s2,s1);
        if(1!=fwrite(smbuf,r,1,fh))
          bret(-18);
      }

    } // show blocks

    if(skip)
      continue;

    //
    //
    //
    
    if(lnk->t!=OBJTYPE_SZ)
      continue;
    osz=(ObjSZ*)lnk;
    
    switch(osz->metaType) {
    default:
      break;
    case METATOK_BETA:
    case METATOK_NONE:
    case METATOK_INCLUDE:
    case METATOK_LIB:

      for(sz=osz->sz,i=0;i<osz->szNum;i++,sz++) {
        
        if((osz->metaType==METATOK_BETA)&&oconfig->betaPass) {
          chr buf[256];
          flagbeta=1;
          r=szfmt(buf,sizeof(buf),patchBetaStart,sz->refId);
          if(1!=fwrite(buf,r,1,fh))
            bret(-18); 
        }
        if(sz->refId) {
          if((r=scWriteSubstitution(list,0,sz,fh,oconfig))<0)
            goto BAIL;
          if(r==METATOK_SKIP)
            continue;
        } else if(osz->metaType) {
          if((r=scWriteSubstitution(list,osz,sz,fh,oconfig))<0)
            goto BAIL;
          continue;
        }
  
        if((r=szlen(sz->str,sz->strx))) {
          if(1!=fwrite(sz->str,r,1,fh))
            bret(-18);
        }
          
        if(flagbeta) {
          flagbeta=0;
          r=szlen(patchBetaEnd,0);
          if(1!=fwrite(patchBetaEnd,r,1,fh))
            bret(-18); 
        }
  
      } // for
    
    } // switch
  } // for

  r=0;
BAIL:
  if(fh)
    fclose(fh);
  return r;
} // scWriteCode()

static cchr *patchLibH1 = "#include \"%s\"\n";

// JFL 16 Nov 19
static int scWriteLibFile(ObjConfig *oconfig,formpath_t formpath,
  cchr *srcbuf,uns32 off1,uns32 off2)
{
  int r,s,bufsize;
  FILE *fh=0;
  cchr *p;
  chr *bufalloc=0;
  
  if(off1>=off2)
    bret(-10);

  if((r=scFormPath(oconfig,formpath))<0)
    goto BAIL;
  if(!(fh=fopen(oconfig->buf,"wb")))
    bret(-11);

  switch(formpath) {
  default: bret(-7);

  case FORMPATH_LIBH:
    oconfig->filesCreated|=M_FILESCREATED_LIBH;
    s=off2-off1;
    p=srcbuf+off1;
    if(1!=fwrite(p,s,1,fh))
      bret(-19);
    break;

  case FORMPATH_LIBLANG:
    oconfig->filesCreated|=M_FILESCREATED_LIBLANG;
    if((r=scFormPath(oconfig,FORMPATH_LIBH))<0)
      goto BAIL;
    bufsize=szsize(oconfig->buf,0)+256;
    if((r=MemAlloc(&bufalloc,bufsize))<0)
      goto BAIL;

    r=szfmt(bufalloc,bufsize,patchLibH1,oconfig->buf);
    if(1!=fwrite(bufalloc,r,1,fh))
      bret(-18);
  
    s=off2-off1;
    p=srcbuf+off1;
    if(1!=fwrite(p,s,1,fh))
      bret(-19);
    break;
  } // switch
  
  r=0;
BAIL:
  if(bufalloc)
    MemFree(bufalloc);
  if(fh)
    fclose(fh);
  return r;
} // scWriteLibFile()

// JFL 16 Nov 19
static int scWriteLibCode(BaseLink *list)
{
  int r;
  ObjConfig *oconfig;
  BaseLink *lnk;
  ObjMark *omark=0,*omark2;
  chr *bufsrc=0;

  if(!(oconfig=scGetConfig(list)))
    bret(-1);

  if((r=scFormPath(oconfig,FORMPATH_MAINLANG))<0)
    goto BAIL;
  if((r=BaseReadFile(oconfig->buf,0,&bufsrc,0))<0)
    goto BAIL;
    
  // find include & code sections for the lib

  for(lnk=list->n;lnk->t;lnk=lnk->n) {
    if(lnk->t!=OBJTYPE_Mark) continue;
    omark2=omark;
    omark=(ObjMark*)lnk;
    if(!omark2) continue;
    if(omark2->mType1 == BLOCKTYPE_INCLUDELIB) {
      PRINT_COM_VRB("--includelib--\n");
      if((r=scWriteLibFile(oconfig,FORMPATH_LIBH,
        bufsrc,omark2->fhOff,omark->fhOff))<0)
        goto BAIL;
    } else if(omark2->mType1 == BLOCKTYPE_FUNCTIONLIB) {
      PRINT_COM_VRB("--functionlib--\n");
      if((r=scWriteLibFile(oconfig,FORMPATH_LIBLANG,
        bufsrc,omark2->fhOff,omark->fhOff))<0)
        goto BAIL;
    }
  } // for

  r=0;
BAIL:
  if(bufsrc)
    MemFree(bufsrc);

  return r;
} // scWriteLibCode()
  
// JFL 16 Sep 19
static int scCompile(BaseLink *list)
{
  int r,s,bs;
  ObjConfig *oconfig;
  cchr *compiler,*compilerx;
  chr *cmdalloc=0,*bb;
  cchr *ext,*s1,*s2,*libup,*libupx;
  BaseLink *lnk;
  ObjSZ *osz;
  ObjSZOne *sz,*szx;

  if(!(oconfig=scGetConfig(list)))
    bret(-1);

  //
  // COMPILE
  //
  
  // find path to directory one up from the library folder
  libup=oconfig->sclib;
//  if((libupx=szrchr(libup,oconfig->sclibx,'\\'))
//    ||(libupx=szrchr(libup,oconfig->sclibx,'/'))) {
//    if(szlen(libupx,oconfig->sclibx)<2) {
//      s1=libupx-1;
//      if((libupx=szrchr(libup,s1,'\\'))
//        ||(libupx=szrchr(libup,s1,'/')))
//        /* do nothing */;
//    }
//  }
  libupx=szlastslash(oconfig->sclib,oconfig->sclibx);
  if(szlen(libupx,oconfig->sclibx)<2) {
    s1=libupx-1;
    libupx=szlastslash(libup,s1);
  }
  //if(s1!=libupx) BRK();

  // compiler
  if(oconfig->flags&M_OBJCONFIG_ISCPP)
    compiler=oconfig->ccpath,compilerx=oconfig->ccpathx;
  else
    compiler=oconfig->cpath,compilerx=oconfig->cpathx;

  s = szsize(compiler,compilerx);
  s += szsize(oconfig->buf,0);
  if(oconfig->numMetaTable[METATOK_LIB]) {
    s += szsize(libup,libupx);
    s += 256 * oconfig->numMetaTable[METATOK_LIB]; // todo: get lib name size
  }
  s += 1024;
  if((r=MemAlloc(&cmdalloc,s))<0)
    goto BAIL;
  bb = cmdalloc,bs=s;

  r=szfmt(bb,bs,"%S",compiler,compilerx);
  bb+=r,bs-=r;
  
  // C/C++ STANDARD
  if(!oconfig->cFlagStd[0]) {
    if(oconfig->flags&M_OBJCONFIG_ISCPP)
      s1=oconfig->ccstd,s2=oconfig->ccstdx;
    else
      s1=oconfig->cstd,s2=oconfig->cstdx;
    szcpy(oconfig->cFlagStd,sizeof(oconfig->cFlagStd),s1,s2);
  }
  if((oconfig->cFlagStd[0]=='C')||((oconfig->cFlagStd[0]=='c'))) {
    r=szfmt(bb,bs," -std=%s",oconfig->cFlagStd);
    bb+=r,bs-=r;
  }

  if(oconfig->debug) {
    r=szfmt(bb,bs," -g");
    bb+=r,bs-=r; 
  }

  if(oconfig->flags&M_OBJCONFIG_ISCPP) {
    if(oconfig->ccflags) {
      r=szfmt(bb,bs," %S",oconfig->ccflags,oconfig->ccflagsx);
      bb+=r,bs-=r;
    }
  } else {
    if(oconfig->cflags) {
      r=szfmt(bb,bs," %S",oconfig->cflags,oconfig->cflagsx);
      bb+=r,bs-=r;
    }    
  }

  // if there are any libs, add include path
  if(oconfig->numMetaTable[METATOK_LIB]) {
    // add include path
    r=szfmt(bb,bs," -I%S",libup,libupx);
    bb+=r,bs-=r;

    // add lib path
    r=szfmt(bb,bs," -L%S",oconfig->sclib,oconfig->sclibx);
    bb+=r,bs-=r;
  }
  
  //
  // CFLAGS
  //
  
  if(oconfig->numMetaTable[METATOK_CFLAGS]) {
    for(lnk=list->n;lnk->t;lnk=lnk->n) {
      if(lnk->t!=OBJTYPE_SZ) continue;
      osz=(ObjSZ*)lnk;
      if(osz->metaType!=METATOK_CFLAGS) continue;
      for(sz=osz->sz,szx=sz+osz->szNum;sz!=szx;sz++) {
        r=szfmt(bb,bs," %S",sz->str,sz->strx);
        bb+=r,bs-=r;
      } // for
    } // for
  } // CFLAGS

  //
  //
  //

  cchr *dash=oconfig->betaPass?oconfig->dashExtBeta:oconfig->dashExtMain;
  ext = (oconfig->flags&M_OBJCONFIG_ISCPP)?extCPP2:extC2;
  szfmt(oconfig->dot,oconfig->dotToEndSize,"%s.%s",dash,ext);
  r=szfmt(bb,bs," %s",oconfig->buf);
  bb+=r,bs-=r;

  // link in libs
  if(oconfig->numMetaTable[METATOK_LIB]) {
    for(lnk=list->n;lnk->t;lnk=lnk->n) {
      if(lnk->t!=OBJTYPE_SZ)
        continue;
      osz=(ObjSZ*)lnk;
      if(osz->metaType!=METATOK_LIB)
        continue;
      if(!osz->szNum)
        continue;
      sz=&osz->sz[0];
      
      r=szfmt(bb,bs," -l%S",sz->str,sz->strx);
      bb+=r,bs-=r;
    
    } // for   
  } // sclib
  
  // linker flags
  if(oconfig->flags&M_OBJCONFIG_ISCPP) {
    if(oconfig->ldccflags) {
      r=szfmt(bb,bs," %S",oconfig->ldccflags,oconfig->ldccflagsx);
      bb+=r,bs-=r;
    } else {    
      r=szfmt(bb,bs," -lstdc++");
      bb+=r,bs-=r;
    }      
  } else {
    if(oconfig->ldcflags) {
      r=szfmt(bb,bs," %S",oconfig->ldcflags,oconfig->ldcflagsx);
      bb+=r,bs-=r;
    }
  }

  //
  // LFLAGS
  //
  
  if(oconfig->numMetaTable[METATOK_LFLAGS]) {
    for(lnk=list->n;lnk->t;lnk=lnk->n) {
      if(lnk->t!=OBJTYPE_SZ) continue;
      osz=(ObjSZ*)lnk;
      if(osz->metaType!=METATOK_LFLAGS) continue;
      for(sz=osz->sz,szx=sz+osz->szNum;sz!=szx;sz++) {
        r=szfmt(bb,bs," %S",sz->str,sz->strx);
        bb+=r,bs-=r;
      } // for
    } // for
  } // LFLAGS
  
  szfmt(oconfig->dot,oconfig->dotToEndSize,"%s.%s",dash,extExe2);
  r=szfmt(bb,bs," -o %s",oconfig->buf);
  bb+=r,bs-=r;

  oconfig->filesCreated|=M_FILESCREATED_MAINEXE;
  PRINT_SYS_VRB("C> %s\n",cmdalloc);
  if((r=BaseRunScript(0,cmdalloc,0,1)))
    goto BAIL;

BAIL:
  if(r)
    PRINT_SYS_ERR("* %s\n",cmdalloc);
  if(cmdalloc)
    MemFree(cmdalloc);
  return r;
} // scCompile()

// JFL 16 Nov 19
static int scCompileLib(BaseLink *list)
{
  int r,cmdsize,bs;
  ObjConfig *oconfig;
  cchr *compiler,*compilerx,*s1;
  cchr *ar,*arx;
  chr *bb,*cmdalloc=0;
  
  if(!(oconfig=scGetConfig(list)))
    bret(-1);
    
  // compiler
  if(oconfig->flags&M_OBJCONFIG_ISCPP)
    compiler=oconfig->ccpath,compilerx=oconfig->ccpathx;
  else
    compiler=oconfig->cpath,compilerx=oconfig->cpathx;
    
  ar=oconfig->arpath,arx=oconfig->arpathx;
    
  // language file
  if((r=scFormPath(oconfig,FORMPATH_LIBLANG))<0)
    goto BAIL;

  cmdsize = szsize(compiler,compilerx);
  cmdsize += 2*szsize(oconfig->buf,0);
  cmdsize += 1024;
  if((r=MemAlloc(&cmdalloc,cmdsize))<0)
    goto BAIL;
    
  //
  // COMPILE
  //
  
  bb=cmdalloc,bs=cmdsize;

  r=szfmt(bb,bs,"%S",compiler,compilerx);
  bb+=r,bs-=r;
  
  r=szfmt(bb,bs," -c %s",oconfig->buf);
  bb+=r,bs-=r;

  s1=szlastslash(oconfig->buf,0);
  r=szfmt(bb,bs," -I. -I%S"DIRSLASH,oconfig->buf,s1);
  bb+=r,bs-=r;
  
  if((r=scFormPath(oconfig,FORMPATH_LIBO))<0)
    goto BAIL;
  r=szfmt(bb,bs," -o %s",oconfig->buf);
  bb+=r,bs-=r;

  oconfig->filesCreated|=M_FILESCREATED_LIBO;
  PRINT_SYS_VRB("C> %s\n",cmdalloc);
  if((r=BaseRunScript(0,cmdalloc,0,1)))
    goto BAIL;
    
  //
  // BUILD LIB
  //
  
  bb=cmdalloc,bs=cmdsize;
  
  r=szfmt(bb,bs,"%S %S",ar,arx,oconfig->arflags,oconfig->arflagsx);
  bb+=r,bs-=r;
  
  if((r=scFormPath(oconfig,FORMPATH_LIBA))<0)
    goto BAIL;
  r=szfmt(bb,bs," %s",oconfig->buf);
  bb+=r,bs-=r;

  if((r=scFormPath(oconfig,FORMPATH_LIBO))<0)
    goto BAIL;
  r=szfmt(bb,bs," %s",oconfig->buf);
  bb+=r,bs-=r;
  
  oconfig->filesCreated|=M_FILESCREATED_LIBA;
  PRINT_SYS_VRB("A> %s\n",cmdalloc);
  if((r=BaseRunScript(0,cmdalloc,0,1)))
    goto BAIL;

  r=0;
BAIL:
  if(cmdalloc)
    MemFree(cmdalloc);
  return r;
} // scCompileLib()

// JFL 16 Sep 19
static int scConfig(BaseLink *list,cchr *scpath,cchr *scpathx,int argc,cchr *argv[])
{
  int r,s;
  ObjConfig *oconfig;

  if(!scpath || !szlen(scpath,scpathx))
    ret(-1);

  //
  // ALLOC
  //
  
  // alloc to work on file name
  s = szsize(scpath,scpathx)+512;
  if((r=ObjConfigNew(&oconfig,s))<0)
    goto BAIL;
  BaseLinkBefore(list,BL(oconfig));

  r=0;
  #if !BUILD_MINGW32
  // some systems can't need ./ to run from same dir
  if(!szlastslash(scpath,scpathx)) {
    r=szcpy(oconfig->buf,oconfig->bufSize,"./",0);
  }
  #endif // !BUILD_MINGW32

  szcpy(oconfig->buf+r,oconfig->bufSize-r,scpath,scpathx);

  oconfig->argc=argc;
  oconfig->argv=argv;
  

  
  r = 0;
BAIL:
  return r;
} // scConfig()

// JFL 16 Sep 19
static int scCheckTimes(BaseLink *list)
{
  int r;
  cchr *s1,*bufx;
  uns64 seca64,secb64;
  ObjConfig *oconfig;
  
  if(!(oconfig=scGetConfig(list)))
    bret(-1);

  //
  // FILE NAMES
  //
  
  if(oconfig->dot) BRK(); // only do this once..
  bufx=oconfig->buf+szlen(oconfig->buf,0);

  if(!(s1=szlastslash(oconfig->buf,bufx)))
    s1=oconfig->buf;
  if(!(oconfig->dot=(chr*)szrchr(s1,bufx,'.')))
    oconfig->dot=((chr*)oconfig->buf)+szlen(oconfig->buf,0);
  szcpy(oconfig->dashExtMain,sizeof(oconfig->dashExtMain),dashSC,0);
  szcpy(oconfig->dashExtBeta,sizeof(oconfig->dashExtBeta),dashBeta,0);
  szcpy(oconfig->dashExtLib,sizeof(oconfig->dashExtLib),dashLib,0);
  
  oconfig->dotToEndSize=PTR_DIFF(oconfig->buf+oconfig->bufSize,oconfig->dot);
  
  oconfig->name = oconfig->buf;
  bufx=oconfig->buf+szlen(oconfig->buf,0);
  if((s1=szrchr(oconfig->name,bufx,'/')) || (s1=szrchr(oconfig->name,bufx,'\\')))
    oconfig->name=(chr*)(s1+1);

  // check extension for C or CPP
  if(szisub("cc",0,oconfig->dot,bufx) || szisub("cpp",0,oconfig->dot,bufx))
    oconfig->flags|=M_OBJCONFIG_ISCPP;
  
  //
  // CHECK IF SCRIPT IS NEWER
  //

  if((r=BaseFileDate(&seca64,oconfig->buf,0))<0)
    seca64=0;

  //szfmt(oconfig->dot,sizeof(extExe)+1,"%s",extExe);
  szfmt(oconfig->dot,oconfig->dotToEndSize,"%s.%s",oconfig->dashExtMain,extExe2);
  if((r=BaseFileDate(&secb64,oconfig->buf,0))<0)
    secb64=0;

  //printf("%llu %llu\n",seca64,secb64);
  
  r = (seca64<secb64) ? 1 : 0; // 1 = no need to build
BAIL:
  return r;
} // scCheckTimes()

#define ARGV_START 2

// JFL 19 Sep 19
static int scRun(BaseLink *list)
{
  int r,i,bs;
  ObjConfig *oconfig;
  chr *bb,*cmdline=0;
  cchr **argv,*s1;

  if(!(oconfig=scGetConfig(list)))
    bret(-1);
  argv=oconfig->argv; // get argv passed in
    
  // SIZE & ALLOC

  bs=szsize(oconfig->buf,0);
  bs+=oconfig->dotToEndSize;
  
  for(i=ARGV_START;i<oconfig->argc;i++) {
    s1 = argv[i];
    bs+=szsize(s1,0);
  }

  if((r=MemAlloz(&cmdline,bs))<0)
    goto BAIL;
  bb=cmdline;
  
  // COMMAND LINE
  
  //if(!szcountchrs(oconfig->buf,oconfig->dot,"\\/",0)) {
  //  r=szfmt(bb,bs,"./");
  //  bb+=r,bs-=r;
  //}

  r=szfmt(bb,bs,"%S%s.%s",oconfig->buf,oconfig->dot,oconfig->dashExtMain,extExe2);
  bb+=r,bs-=r;
  
  // don't add script to path - it will run the same as a script and as exe
  for(i=ARGV_START;i<oconfig->argc;i++) {
    s1 = argv[i];
    r=szfmt(bb,bs," %s",s1);
    bb+=r,bs-=r;
  } //for
  
  if(oconfig->flags&M_OBJCONFIG_LAUNCH) {
    PRINT_SYS_VRB("L> '%s'\n",cmdline);
    if((r=BaseLaunchScript(cmdline,0,0)))
      goto BAIL;
  } else {
    PRINT_SYS_VRB("R> '%s'\n",cmdline);
    if((r=BaseRunScript(0,cmdline,0,1)))
      goto BAIL;
  }
  
  r=0;
BAIL:
  if(cmdline)
    MemFree(cmdline);
  return r;
} // scRun()

// JFL 22 Oct 19
static int scFindLineOfFileMark(BaseLink *list,ObjConfig *oconfig,chr **allocp,uns32 mark)
{
  int r;
  cchr *s1;
  chr *alloc=0;
  if(!allocp)
    bret(-1);
  if(!(alloc=*allocp)) {
  
    cchr *ext = (oconfig->flags&M_OBJCONFIG_ISCPP)?extCPP2:extC2;
    szfmt(oconfig->dot,oconfig->dotToEndSize,"%s.%s",oconfig->dashExtMain,ext);
    if((r=BaseReadFile(oconfig->buf,0,&alloc,0))<0)
      goto BAIL;
    *allocp=alloc;
  }
  s1=alloc+mark; // should be OK if it runs past end of buf
  r=szcountlines(alloc,s1);
BAIL:
  return r;
} // scFineLineOfFileMark()

// JFL 10 Nov 19
static int scBuildArgArr(void *outp,cchr *str,cchr *strx)
{ // build zero-terminated array of pointers to strings
  int r,num,spt,sst;
  chr **pt,**ptx;
  chr *ss,*ssx;
  void *alloc=0;
  chr c;

  if(!strx) strx=str+szlen(str,0);
  sst=szsize(str,strx);
  if(sst<2)
    ret(0);
  num=szcountchrs(str,strx,";",0)+2;
  spt = num*sizeof(*pt);
  
  if((r=MemAlloc(&alloc, spt + sst + 1))<0)
    goto BAIL;
  pt = alloc;
  ptx = pt + num;
  ss = PTR_ADD(pt, spt);
  ssx = PTR_ADD(alloc,spt+sst);
  
  *pt++ = ss;
  for(;ss<ssx;) {
    if(!(c=*str++) || (str>=strx))
      break;
    if(c==';') c=0;

    if(!(*ss++=c)) {
      *pt++=ss;
    }

  } // for
  *ss=0;
  *pt=0;
  
  if(pt>=ptx)
    bret(-1);
  
  if(outp)
    *((void**)outp)=alloc,alloc=0;

  r=0;
BAIL:
  if(alloc)
    MemFree(alloc);
  return r;
} // scBuildArgArr()

// JFL 19 Sep 19
static int scDebug(BaseLink *list)
{
  int r,i,bs;
  ObjConfig *oconfig;
  chr *bb,*cmdline=0;
  cchr **argv,*s1;
  chr *filealloc=0;
  BaseLink *lnk;
  ObjSZ *osz;
  ObjSZOne *sz,*szx;

  if(!(oconfig=scGetConfig(list)))
    bret(-1);
  argv=oconfig->argv;
    
  bs=512;
  bs+=szsize(oconfig->dbpath,oconfig->dbpathx);
  bs+=szsize(oconfig->buf,0);
  bs+=oconfig->dotToEndSize;
  
  for(i=ARGV_START;i<oconfig->argc;i++) {
    s1 = argv[i];
    bs+=szsize(s1,0);
  } // for

  bs += oconfig->numMetaTable[METATOK_GDB]*512; // todo: find size

  if((r=MemAlloz(&cmdline,bs))<0)
    goto BAIL;
  bb=cmdline;

  // path/to/gdb
  r=szfmt(bb,bs,"%S",oconfig->dbpath,oconfig->dbpathx);
  bb+=r,bs-=r;
  
  if(oconfig->dbarg) {
    r=szfmt(bb,bs," %S",oconfig->dbarg,oconfig->dbargx);
    bb+=r,bs-=r;
  }

  // execute break
  if(oconfig->numMetaTable[METATOK_GDB]) {
    for(lnk=list->n;lnk->t;lnk=lnk->n) {
      if(lnk->t!=OBJTYPE_SZ) continue;
      osz=(ObjSZ*)lnk;
      if(osz->metaType!=METATOK_GDB) continue;
      for(sz=osz->sz,szx=sz+osz->szNum;sz!=szx;sz++) {
        // if break is only word, break at the line
        s1=sztrimwhite(sz->str,sz->strx);
        if(!szicmp("break",0,sz->str,s1)) {
          // break -- change to: b <linenumber>
          ObjSZOne const *sz2;
          if((r=scFindSZRef(&sz2,list,0,sz->refId|M_BLOCKID_REF))<0)
            continue;
          if((r=scFindLineOfFileMark(list,oconfig,&filealloc,sz2->fileOff))<0)
            continue;
          i=r;
          r=szfmt(bb,bs," -ex \"b %d\"",i);
        } else {
          // add the command
          r=szfmt(bb,bs," -ex \"%S\"",sz->str,sz->strx);
        }
        bb+=r,bs-=r;        
      } // for
    } // for
  } else {
    r=szfmt(bb,bs," -ex \"b main\"");
    bb+=r,bs-=r;
  }
  
  // run
  r=szfmt(bb,bs," -ex r --args %S%s.%s",
    oconfig->buf,oconfig->dot,oconfig->dashExtMain,extExe2);
  bb+=r,bs-=r;

  // args
  if(oconfig->argc>1) {
    for(i=ARGV_START;i<oconfig->argc;i++) {
      s1 = argv[i];
      //printf("[%d] '%s'\n",i,s1);
      r=szfmt(bb,bs," %s",s1);
      bb+=r,bs-=r;
    } // for
  }

  if(szisub("-tui",0,cmdline,0))
    oconfig->flags|=M_OBJCONFIG_LAUNCH;

  if(oconfig->flags&M_OBJCONFIG_LAUNCH) {
    //chr* envarr[]={"TERM=xterm-256color",0}
    void *argarr=0;
    if((r=scBuildArgArr(&argarr,oconfig->dbenv,oconfig->dbenvx))<0)
      ; // do nothing
    PRINT_COM_VRB("L> %s\n",cmdline);
    r=BaseLaunchScript(cmdline,0,argarr);
    if(argarr)
      MemFree(argarr);
    if(r<0)
      goto BAIL;
  } else {
    PRINT_COM_VRB("R> %s\n",cmdline);
    if((r=BaseRunScript(0,cmdline,0,2)))
      goto BAIL;
  }

  r=0;
BAIL:
  if(filealloc)
    MemFree(filealloc);
  if(cmdline)
    MemFree(cmdline);
  return r;
} // scDebug()

////////////////////////////////////////////////////////////////////////////////
// RC FILE

#define RCTOKS_X \
  X(NONE) \
  X(ARFLAGS) \
  X(ARPATH) \
  X(CCFLAGS) \
  X(CCPATH) \
  X(CCSTD) \
  X(CFLAGS) \
  X(CPATH) \
  X(CSTD) \
  X(DBARG) \
  X(DBENV) \
  X(DBPATH) \
  X(ELSE) \
  X(ENDIF) \
  X(IF) \
  X(IFNOT) \
  X(LDCCFLAGS) \
  X(LDCFLAGS) \
  X(PRINT) \
  X(SCDOC) \
  X(SCLIB) \
  X(SET) \
  X(ZMAX)

#define X(a) RCTOK_##a,
typedef enum { RCTOKS_X } rctok_enum;
#undef X

#define X(a) #a,
static cchr *rcTokTable[] = { RCTOKS_X 0 };
#undef X

#define RCDEFS_X \
  X(NONE) \
  X(BUILD_MINGW32) \
  X(BUILD_LINUX) \
  X(DEBUG) \
  X(VERBOSE)

#ifndef NONE
#define NONE 0
#endif // NONE

#ifndef BUILD_LINUX
#define BUILD_LINUX 0
#endif // BUILD_LINUX

#ifndef BUILD_MINGW32
#define BUILD_MINGW32 0
#endif // BUILD_MINGW32

#ifndef DEBUG
#define DEBUG 0
#endif // DEBUG

#ifndef VERBOSE
#define VERBOSE 0
#endif // VERBOSE

#define RCDEFSYMS_X \
  XX(CPP,"c++") \
  XX(C,"c")

#define XX(a,b) RCDEF_##a,
#define X(a) RCDEF_##a,
enum { RCDEFS_X RCDEFSYMS_X };
#undef X
#undef XX

#define XX(a,b) b,
#define X(a) #a,
static cchr* rcDefTokTable[] = { RCDEFS_X RCDEFSYMS_X 0 };
#undef X
#undef XX

#define XX(a,b) 0,
#define X(a) a,
static int rcDefValTable[] = { RCDEFS_X RCDEFSYMS_X };
#undef X
#undef XX

// JFL 11 Oct 19
// JFL 19 Nov 19
static int scScanRC(BaseLink *list,chr *buf,int bufSize,cchr *path,cchr *pathx)
{
  int r,tok,skip=0;
  cchr *s1,*s2,*s3,*sx,*eol,*rcbuf,*rcbufx;
  ObjBuf *obuf;
  ObjConfig *oconfig;
  
  if(!(oconfig=scGetConfig(list)))
    bret(-1);

  if((r=ObjBufNew(&obuf,0))<0)
    goto BAIL;
  BaseLinkBefore(list,BL(obuf));
  rcbuf=buf,rcbufx=buf+bufSize;
  obuf->buf=buf; // pass ownership
  obuf->flagFree=1; // request to be freed

  if(oconfig->debug) // override if set on command line
    rcDefValTable[RCDEF_DEBUG]=1;

  //
  //
  //

  for(eol=rcbuf;;) {
    s1=szskipwhite(eol,rcbufx);
    if(szeos(s1,rcbufx))
      break;
    eol=sztilleol(s1,rcbufx);

    sx=szsub("//",0,s1,eol);
    if(!sx)
      sx=eol;
    
    if(s1>=sx)
      continue;

    s2=sztillwhite(s1,sx);
    tok=szimap(rcTokTable,s1,s2);
    s3=szskipwhite(s2,sx);
    
    if(skip && (tok!=RCTOK_ENDIF) && (tok!=RCTOK_ELSE))
      continue;

    switch(tok) {
    default:
      PRINT_SYS_ERR("rc syntax: %S, line:%d, '%S'\n",
        path,pathx,szcountlines(buf,s2),s1,s2);
      break;
    case RCTOK_SET:
      s2=sztillwhite(s3,sx);
      r=szimap(rcDefTokTable,s3,s2);
      if((r<0)||(r>NUM(rcDefValTable))) {
        sx=s2;
        goto unknownval;
      }
      // override if set from command line
      if((r==RCDEF_DEBUG) && oconfig->debug)
        break;
      s2=szskipwhite(s2,sx);
      sztobin(s2,sx,10,'i',&rcDefValTable[r]);
      break;
    case RCTOK_IF:
    case RCTOK_IFNOT:
      sx=sztrimwhite(s3,sx);
      r=szimap(rcDefTokTable,s3,sx);
      if((r<0)||(r>NUM(rcDefValTable))) {
      unknownval:
        PRINT_SYS_ERR("rc unknown-token: %S, line:%d, '%S'\n",
        path,pathx,szcountlines(buf,s2),s3,sx);
        break;
      }
      if(rcDefValTable[r]) {
        if(tok==RCTOK_IFNOT)
          skip=1;
      } else {
        if(tok==RCTOK_IF)
          skip=1;
      }
      break;
    case RCTOK_ELSE:
      skip = skip?0:1;
      break;
    case RCTOK_ENDIF:
      if(skip)
        skip=0;
      break;
    case RCTOK_CPATH:
      oconfig->cpath=s3,oconfig->cpathx=sztrimwhite(s3,sx);
      break;
    case RCTOK_CCPATH:
      oconfig->ccpath=s3,oconfig->ccpathx=sztrimwhite(s3,sx);
      break;
    case RCTOK_ARPATH:
      oconfig->arpath=s3,oconfig->arpathx=sztrimwhite(s3,sx);
      break;
    case RCTOK_ARFLAGS:
      oconfig->arflags=s3,oconfig->arflagsx=sztrimwhite(s3,sx);
      break;
    case RCTOK_DBPATH:
      oconfig->dbpath=s3,oconfig->dbpathx=sztrimwhite(s3,sx);
      break;
    case RCTOK_SCLIB:
      oconfig->sclib=s3,oconfig->sclibx=sztrimwhite(s3,sx);
      s2=szlastslash(s3,sx),oconfig->sclibrelx=sztrimwhite(s3,sx);
      oconfig->sclibrel=szlastslash(s3,s2);
      if(oconfig->sclibrel && ((*(oconfig->sclibrel)=='/') || (*(oconfig->sclibrel)=='\\')))
        oconfig->sclibrel++;
      break;
    case RCTOK_CFLAGS:
      oconfig->cflags=s3,oconfig->cflagsx=sztrimwhite(s3,sx);
      break;
    case RCTOK_CCFLAGS:
      oconfig->ccflags=s3,oconfig->ccflagsx=sztrimwhite(s3,sx);
      break;
    case RCTOK_LDCFLAGS:
      oconfig->ldcflags=s3,oconfig->ldcflagsx=sztrimwhite(s3,sx);
      break;
    case RCTOK_LDCCFLAGS:
      oconfig->ldccflags=s3,oconfig->ldccflagsx=sztrimwhite(s3,sx);
      break;
    case RCTOK_CSTD:
      oconfig->cstd=s3,oconfig->cstdx=sztrimwhite(s3,sx);
      break;
    case RCTOK_CCSTD:
      oconfig->ccstd=s3,oconfig->ccstdx=sztrimwhite(s3,sx);
      break;
    case RCTOK_SCDOC:
      oconfig->scdoc=s3,oconfig->scdocx=sztrimwhite(s3,sx);
      break;
    case RCTOK_DBARG:
      oconfig->dbarg=s3,oconfig->dbargx=sztrimwhite(s3,sx);
      break;
    case RCTOK_DBENV:
      oconfig->dbenv=s3,oconfig->dbenvx=sztrimwhite(s3,sx);
      break;
    case RCTOK_PRINT:
      PRINT_SYS_MSG("%S\n",s3,sx);
      break;
    } // switch
  } // for
  
  oconfig->debug=rcDefValTable[RCDEF_DEBUG];
  oconfig->verbose=rcDefValTable[RCDEF_VERBOSE];
  
  r=0;
BAIL:
  return r;
} // scScanRC()

// JFL 20 Oct 19
static int scMetaSet(BaseLink *list,cchr *orgbuf,cchr *str,cchr *strx)
{
  int r,tok;
  cchr *s1;
  ObjConfig *oconfig;
  
  if(!(oconfig=scGetConfig(list)))
    bret(-1);
  s1=szskipwhite(str,strx);
  str=sztillwhite(s1,strx);
  tok=szimap(rcDefTokTable,s1,str);
  switch(tok) {
  default:
    // look for matching C standards
    if((*s1=='c')||(*s1=='C')) {
      if((s1[1]=='+')&&(s1[2]=='+')) {
        szcpy(oconfig->cFlagStd,sizeof(oconfig->cFlagStd),s1,str);
        oconfig->flags|=M_OBJCONFIG_ISCPP;
        break;
      } else if(C_IS_DIGIT(s1[1])&&(C_IS_DIGIT(s1[2]))) {
        szcpy(oconfig->cFlagStd,sizeof(oconfig->cFlagStd),s1,str);
        oconfig->flags&=~M_OBJCONFIG_ISCPP;
        break;
      }
    } // look for C standad
    PRINT_SYS_ERR("unknown set-token '%S' (%d)\n",
      s1,str,szcountlines(orgbuf,s1));
    ret(-1);
  case RCDEF_CPP:
    oconfig->flags|=M_OBJCONFIG_ISCPP;
    break;
  case RCDEF_C:
    oconfig->flags&=~M_OBJCONFIG_ISCPP;
    break;
  } // switch
    
  r=0;
BAIL:
  return r;
} // scMetaSet()

// JFL 11 Oct 19
static int scLoadRC(BaseLink *list,cchr *scpath)
{
  int r,s;
  chr buf[FILEPATH_BUF_SIZE];
  chr *loadbuf=0;
  cchr *s1,*sx;

  // try home
  if((r=BaseHomeDir(buf,sizeof(buf)))>0) {
    szfmt(buf+r,sizeof(buf)-r,"/%s",rcfile);
    PRINT_SYS_VRB("config file '%s'\n",buf);
    if((s=BaseReadFile(buf,0,&loadbuf,M_BASEREADFILE_QUIET))>0) {
      scScanRC(list,loadbuf,s,buf,0);
      loadbuf=0;
    }
  }

  // try script dir
  sx=scpath+szlen(scpath,0);
  if((s1=szrchr(scpath,sx,'/'))||(s1=szrchr(scpath,sx,'\\'))) {
    szfmt(buf,sizeof(buf),"%S/%s",scpath,s1,rcfile);
    PRINT_SYS_VRB("config file '%s'\n",buf);
    if((s=BaseReadFile(buf,0,&loadbuf,M_BASEREADFILE_QUIET))>0) {
      scScanRC(list,loadbuf,s,buf,0);
      loadbuf=0;
    }
  }
  
  #ifdef DEV_RCFILE
  r=szfmt(buf,sizeof(buf),DEV_RCFILE);
  PRINT_SYS_VRB("config file '%s'\n",buf);
  if((s=BaseReadFile(buf,0,&loadbuf,M_BASEREADFILE_QUIET))>0) {
    scScanRC(list,loadbuf,s,buf,0);
    loadbuf=0;
  }
  #endif // ifdef DEV_RCFILE

  r=0;
//BAIL:
  if(loadbuf)
    MemFree(loadbuf);
  return r;
} // scLoadRC()

// JFL 21 Oct 19
static void scCleanTempFiles(BaseLink *list)
{
  ObjConfig *oconfig;
  //cchr *ext;
  
  if(!(oconfig=scGetConfig(list)))
    goto BAIL;
  
  // remove main build file
  if(oconfig->filesCreated&M_FILESCREATED_MAINLANG) {
    if(scFormPath(oconfig,FORMPATH_MAINLANG)>=0)
      unlink(oconfig->buf);
  }
  //ext = (oconfig->flags&M_OBJCONFIG_ISCPP)?extCPP2:extC2;
  //szfmt(oconfig->dot,oconfig->dotToEndSize,"%s.%s",oconfig->dashExtMain,ext);
  //unlink(oconfig->buf);

  // remove beta build & run files
  if(oconfig->filesCreated&M_FILESCREATED_BETALANG) {
    if(scFormPath(oconfig,FORMPATH_BETALANG)>=0)
      unlink(oconfig->buf);
  }
  if(oconfig->filesCreated&M_FILESCREATED_BETAEXE) {
    if(scFormPath(oconfig,FORMPATH_BETAEXE)>=0)
      unlink(oconfig->buf);
  }
  //szfmt(oconfig->dot,oconfig->dotToEndSize,"%s.%s",oconfig->dashExtBeta,ext);
  //unlink(oconfig->buf);
  //szfmt(oconfig->dot,oconfig->dotToEndSize,"%s.%s",oconfig->dashExtBeta,extExe2);
  //unlink(oconfig->buf);

  if(oconfig->filesCreated&M_FILESCREATED_LIBLANG) {
    if(scFormPath(oconfig,FORMPATH_LIBLANG)>=0)
      unlink(oconfig->buf);
  }
  if(oconfig->filesCreated&M_FILESCREATED_LIBO) {
    if(scFormPath(oconfig,FORMPATH_LIBO)>=0)
      unlink(oconfig->buf);
  }
  
BAIL:
  return;
} // scCleanTempFiles()

////////////////////////////////////////////////////////////////////////////////

#define INSTANTCOPTS_X \
  X(NONE,0) \
  X(BUILD,"build, don't run") \
  X(DEBUG,"debug with gdb") \
  X(HELP,"show help") \
  X(LAUNCH,"launch") \
  X(REBUILD,"rebuild and run") \
  X(V,"verbose") \
  X(VERBOSE,"verbose") \
  X(ZMAX,0)

#define X(a,b) INSTANTCOPT_##a,
enum { INSTANTCOPTS_X };
#undef X

#define X(a,b) #a,
static cchr *instantCOptTable[] = { INSTANTCOPTS_X 0 };
#undef X

#define X(a,b) b,
static cchr *instantCOptHelp[] = { INSTANTCOPTS_X 0 };
#undef X

////////////////////////////////////////////////////////////////////////////////

// JFL 31 Oct 19
static int mainDoc(ObjConfig *oconfig,int argc,cchr **argv)
{
  int r,i,bs;
  chr *rundoc=0;
  chr *bb;
  cchr *s1;
  
  // size of all args
  bs=szlen(oconfig->scdoc,oconfig->scdocx);
  if(bs<2) {
    PRINT_SYS_ERR("scdoc not configured in rc file\n");
    ret(1);
  }
  for(i=0;i<argc;i++) {
    s1=argv[i];
    bs+=szlen(s1,0)+16;
  } // for
  
  if((r=MemAlloc(&rundoc,bs))<0)
    goto BAIL;
  bb=rundoc;

  r=szfmt(bb,bs,"%S",oconfig->scdoc,oconfig->scdocx);
  bb+=r,bs-=r;

  for(i=0;i<argc;i++) {
    s1=argv[i];
    r=szfmt(bb,bs," %s",s1);
    bb+=r,bs-=r;
  } // for

  if((r=BaseRunScript(0,rundoc,0,2))<0)
    goto BAIL;

  r=0;
BAIL:
  if(rundoc)
    MemFree(rundoc);
  return r;
} // mainDoc()

// JFL 20 Oct 19
static void mainHelp(cchr *exepath)
{
  int i;
  chr name[32];
  cchr *s1;

  if((s1=szlastslash(exepath,0)))
    s1++;
  else
    s1=exepath;
  printf("%s [options]\n",s1);
  printf("------------------\n");
  
  printf("Options that start with a + are handled by InstantC:\n");
  for(i=0;i<NUM(instantCOptHelp);i++) {
    if(!instantCOptHelp[i]) continue;
    szcpy(name,sizeof(name),instantCOptTable[i],0);
    sztolower(name,0);
    printf(" +%s -- %s\n",name,instantCOptHelp[i]);
  } // for

  printf("--------------------------------\n");
  printf("A C/C++ scripting compiler tool. Version: "MAINVERS_SZ"\n");
  printf("See 'man instantc' for documentation.\n");
  printf("Contact: instantcompiler@gmail.com\n");
  printf("%s\n",MainCoprShort);
} // mainHelp()

////////////////////////////////////////////////////////////////////////////////

// JFL 19 Sep 19
int main(int argc, char **argv)
{
  int r,i,k,argc2=0;
  cchr *scpath=0,*exepath,*s1;
  BaseLink head;
  uns8 flagdontrun=0,flagbuild=0,flagdebug=0,flagvrb=0;
  uns8 flagclean=0,flaghelp=0,flaglaunch=0;
  cchr *argv2[argc];
  ObjConfig *oconfig;
  
  MEMZ(MainG);
  BaseLinkMakeHead(&head);
  
  //
  // ARGUMENTS
  //

  exepath=argv[0];
  for(i=0;i<argc;i++) {
    s1=argv[i];
    if(*s1=='+') { // options for us
      s1++;
      k=szimap(instantCOptTable,s1,0);
      switch(k) {
      default:
        PRINT_SYS_ERR("Option '%s' not recognized\n",s1);
      case INSTANTCOPT_HELP:      
        flaghelp=1;
        break;
      case INSTANTCOPT_BUILD:
        flagbuild=1,flagdontrun=1;
        break;
      case INSTANTCOPT_REBUILD:
        flagbuild=1,flagclean=1;
        break;
      case INSTANTCOPT_DEBUG:
        flagdebug=1;
        break;
      case INSTANTCOPT_V:
      case INSTANTCOPT_VERBOSE:
        flagvrb=1;
        break;
      case INSTANTCOPT_LAUNCH:
        flaglaunch=1;
        break;
      } // switch
      continue;
    } // options for us
    
    argv2[argc2++]=s1;
    
    // script path is first non-option param after script path
    if(!scpath && i && *s1!='-')
      scpath=s1;
  } // for
  
  //
  //
  //

  if(!scpath) {
    mainHelp(exepath);
    ret(1);    
  } else if(flaghelp) {
    if(argc<2) {
      mainHelp(exepath);
      ret(1);     
    }
  } // help message

  // set print level bits (set to not-print)
  for(k=0;k<NUM(BaseG.printLevel);k++) {
    if(!flagvrb)
      BaseG.printLevel[k]|=M_PRINT_MSG|M_PRINT_VRB|M_PRINT_ERR;
  } // for

  if((s1=szlastslash(exepath,0))) {
    PRINT_SYS_MSG("%s v"MAINVERS_SZ", %s\n",s1+1,MainCoprShort);
  }

  if((r=scConfig(&head,scpath,0,argc2,(cchr**)argv2))<0)
    goto BAIL;
  if(!(oconfig=scGetConfig(&head)))
    bret(-1);
  if((r=scLoadRC(&head,scpath))<0)
    goto BAIL;
    
  if(flaghelp) {
    mainDoc(oconfig,argc2,argv2);
    ret(0);
  }
    
  if(flagdebug) { // setup for rc file override
    oconfig->flags|=M_OBJCONFIG_DEBUGGER;
    oconfig->debug=1;
    flagbuild=1;
  }
  if(flaglaunch)
    oconfig->flags|=M_OBJCONFIG_LAUNCH;

  // if no build/debug options set, clean temporary files
  if(!flagdebug && !flagbuild && !flagvrb)
    flagclean=1;

  if((r=scCheckTimes(&head))<0)
    goto BAIL;

  if(!r)
    flagbuild=1;

  if(flagvrb) {
    oconfig->flags|=M_OBJCONFIG_VERBOSE;
  }

  if(flagbuild) { // build

    PRINT_SYS_MSG("building..\n");
    
    if(!oconfig->ccpath || !oconfig->cpath) {
      PRINT_SYS_ERR("CPATH or CCPATH not set - %s probably not found\n",rcfile);
      bret(-55);
    }

    if((r=scAddMarks(&head))<0)
      goto BAIL;
    
    if((r=scScan(&head,scpath,0))<0)
      goto BAIL;
    
    if(oconfig->numMetaTable[METATOK_BETA])
      oconfig->betaPass=1;

    if((r=scPatch(&head))<0)
      goto BAIL;

    for(;;) {
      if((r=scWriteCode(&head))<0)
        goto BAIL;
      if((r=scCompile(&head))<0)
        goto BAIL;
      if(r) // any return from compiling is considered an error
        goto BAIL;
  
      if(!oconfig->betaPass)
        break;
      oconfig->betaPass=0;
    } // for
    
    if(oconfig->libBuild) {
      if((r=scWriteLibCode(&head))<0)
        goto BAIL;
      if((r=scCompileLib(&head))<0)
        goto BAIL;
    } // libPass

  } // build
  
  r=0;
  if(!flagdontrun) {
    PRINT_SYS_MSG("running..\n");
    if(!(oconfig->flags&M_OBJCONFIG_DEBUGGER)) {
      if((r=scRun(&head))<0)
        goto BAIL;
    } else {
      if((r=scDebug(&head))<0)
        goto BAIL;
    }
  } // !flagdontrun
  
  if(flagclean)
    scCleanTempFiles(&head);

  PRINT_SYS_VRB("mainrc:%d\n",r);

  // return the value r
BAIL:
  ObjListZap(&head);
  if((i=MemCount())) {
    PRINT_SYS_ERR("MemCount:%d\n",i);
    BRK();
  }
  return r;
} // main()

// EOF
