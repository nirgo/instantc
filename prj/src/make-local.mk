# file: makefile - Joe Linhoff
APP=../../bin/instantc

.PHONY: all clean help
NODEPS= clean help

ifeq ($(OS), Windows_NT)
UNAME:=Windows
else
UNAME:=$(shell uname -s)
endif

ifneq ($(findstring Window,$(UNAME)),)
WINDOWS=1
endif

all: $(APP)

CC=gcc
LD=gcc
DEL=rm -f

CFLAGS=-I. -O2 -DBUILD_PFM_LIB
LDFLAGS=-O2 -lm

ifeq (1,$(WINDOWS))
CFLAGS+=-DBUILD_CYGWIN
else
CFLAGS+=-DBUILD_LINUX
endif

SRC=$(wildcard *.c) # list of source files

SUFFIXES += .d
DEPFILES=$(patsubst %.c,%.d,$(SRC))

ifeq (0, $(words $(findstring $(MAKECMDGOALS), $(NODEPS))))
-include $(DEPFILES)
endif

$(APP): $(SRC:%.c=%.o)
	@echo building $(APP)
	$(CC) $(SRC:%.c=%.o) $(LDFLAGS) -o $@

%.d: %.c
	$(CC) $(CFLAGS) -MM -MT '$(patsubst %.c,%.o,$<)' $< -MF $@

%.o: %.c %.d %.h
	$(CC) $(CFLAGS) -o $@ -c $<

clean:
	$(DEL) $(APP) *.o *.d $(APP).exe

help:
	@echo make clean -- removes all built files
	@echo make -- build
