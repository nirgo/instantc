# Introduction To InstantC

InstantC works to make C and C++ easier to use by handling the build infrastructure and boilerplate.

Install the project from [gitlab.com/jlinhoff/instantc](https://gitlab.com/jlinhoff/instantc). Documentation is at [instantcompiler.com/](http://www.instantcompiler.com)

## Hello InstantC

Here is the Hello World example in C.

Create the file ```hello.sc```:
```
printf("InstantC Hello!\n");
```

Run the script:
```
instantc hello.sc
```

## Hello InstantC++

Here is the Hello World example in C++.

Create the file ```hello.scc```:
```
std::cout << "InstantC++ Hello!\n";
```

Run the instant-file:
```
instantc hello.scc
```

## Ways To Run

There are multiple ways to run the InstantC source files.

First, set the language of your INSTANTFILE:

To Set C++
- file extension contains a 'cc' or 'cpp'
- and/or the meta-substition inside the file ```$(lang c++)```

To Set C
- file extension does not contain 'cc' or 'cpp'
- and/or the meta-substition inside the file ```$(lang c)```

Second, run your INSTANTFILE:
- run through instantc:
    - ```instantc INSTANTFILE```
- run as a script:
    - (unix-type systems support this)
    - add ```#!/usr/bin/env instantc``` to the first line of INSTANTFILE
    - ```chmod +x INSTANTFILE```
    - ```./INSTANTFILE```

## Early Version Warning

This project and documentation are under active development and subject to big changes!

Documentation Version: <#(versfile introvers.txt)#>

## Under-The-Hood

InstantC builds an intermediate file, compiles the intermediate file, and runs the executable. The intermediate file is normally removed.

To run the build step and see what's going on under-the-hood, run the instant-file with the ```+build``` option:
```
instantc hello.sc +build
```

Look at the intermediate file, ```hello-sc.c```.

Parameters that start with a plus, are kept by InstantC and not passed onto the executable.

## Meta-Substitution Step

InstantC scans the source file and performs substitutions when creating the intermediate file.

Here is a simple meta-substitution example:
```
char *str = "Hello $(emit World!)\n";
```

After the substitution, the code will look like:
```
char *str = "Hello Word!\n";
```

The meta-substitution scanner operates independent of the C or C++ language. It performs a simple pattern-scan of the source for the start and end of meta-substitutions. The dollar-sign-open-paren signals the start, and the close-paren signals the end.

## Compile Step

After the substitutions, the intermediate file is compiled. An executable is created and run.

## Runtime Arguments

Arguments are passed in through the normal array of C-string pointers to main. Use the meta-substitution ```$(ARGV)``` for the pointer-to-array of C-strings, and the number of arguments is ```$(ARGC)```.

Example to print the arguments:
```
printf("InstantC arguments\n");

// list arguments -- $(ARGC) and $(ARGV) are replaced
for(int i=0;i<$(ARGC);i++) {
  printf("[%d] '%s'\n",i,$(ARGV)[i]);
} // for
```

## Debugging

Run the instant-file with the *+debug* option to start GDB and break at main. Debugging is an important skill that that helps you write great code. Look online for info on how to use GDB.

Here are a few GDB commands:
- n ENTER -- to run the next line
- s ENTER -- to step into the next statement
- p VAR ENTER -- to print the value of VAR
- q ENTER-- to exit the debugger

The *.instantc* config file may specifiy ```dbargs -tui``` to run GDB inside a text-based user interface. Remove this line to run GDB from the command line. See the Config File Reference.

## Documentation
- man instantc
- visit [www.instantcompiler.com](http://www.instantcompiler.com)
- bugs, contact: instantcompiler@gmail.com

## Goals

One of my goals is to make C and C++ easier to use. This project handles the infrastructure and boilerplate so you can use C and C++ as you would any other scripting language.

The goal is not to create a new scripting language, but to leverage all the current features and future advances in the languages. InstantC avoids adding any language-specific handling and allow for future extension into other languages.
