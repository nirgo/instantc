#!/usr/bin/env instantc
printf("InstantC example1\n");

// list arguments -- $(ARGC) and $(ARGV) are replaced with actual name
for(int i=0;i<$(ARGC);i++) {
  printf("[%d] '%s'\n",i,$(ARGV)[i]);
} // for

// run shell script
printf("Run $(shell echo "shell script"). To replace code at compile time.\n");

// add include files -- stdio.h is auto-included unless you include any files
// also, meta-substitutions can be grouped on the same line
$(include <stdio.h>) $(include <stdint.h>)
printf("sizeof(intptr_t)=%ld\n",sizeof(intptr_t));
